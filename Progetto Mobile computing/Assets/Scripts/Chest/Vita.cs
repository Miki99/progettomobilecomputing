﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vita : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        //Se il player prende la vita, gli viene incrementata di una.
        if(collision.gameObject.tag.Equals("player"))
        {
            collision.gameObject.GetComponent<Player>().IncrementaVita();
            Destroy(gameObject);
        }
    }
}
