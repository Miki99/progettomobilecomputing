﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Chest : MonoBehaviour
{
    private string[] oggetti = { "chiave", "vita", "bomba", "no_oggetti"};

    /* Ratio degli oggetti: per un oggetto che ha piu' alto e' il valore del ratio, 
     * rispetto agli altri, piu' probabile che spawni tale oggetto. */
    [SerializeField] float ratio_vita = 0;                          //Ratio di spawn della vita.
    [SerializeField] float ratio_key = 0;                           //Ratio di spawn della chiave.
    [SerializeField] float ratio_bomba = 0;                         //Ratio di spawn della bomba.
    [SerializeField] float ratio_noOggetti = 0;                     //Ratio di spawn di nessun oggetto.
    private float ratio_tot;                                        //Ratio totale di spawn di tutti gli oggetti.
    private Dictionary<string, List<int>> oggetto2valori;           //Ogni oggetto ha il suo range di valori di spawn.

    private bool isOpened;                                          //Indica se la chest e' stata aperta o meno.

    private GameObject player;                                      //GameObject del player.

    // Start is called before the first frame update
    void Start()
    {
        ratio_tot = ratio_bomba + ratio_key + ratio_vita + ratio_noOggetti;
        isOpened = false;
        player = GameObject.FindGameObjectWithTag("player").gameObject;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //Il player ha "colliso" alla chest?
        if (collision.gameObject.tag.Equals("player") && !isOpened)
        {
            GetComponent<Animator>().SetBool("Open", true);
            isOpened = true;

            SpawnOggetto();
        }
    }

    /// <summary>
    /// Indica se la chest e' aperta o meno.
    /// </summary>
    /// <returns>true se la chest e' aperta, false altrimenti.</returns>
    public bool isOpen()
    {
        return isOpened;
    }

    /// <summary>
    /// Modifica il ratio di spawn di oggetti di questa chest se sara' l'ultima ad essere aperta e che il player 
    /// non trova ancora la chiave. In questo modo verra' garantito lo spawn della chiave.
    /// </summary>
    private void UltimaChestEdit()
    {
        //Il player possiede la chiave?
        if (player.GetComponent<Player>().HaveKey())
            return;

        //Controlla se questa chest sara' l'ultima ad essere aperta.
        GameObject[] chests = GameObject.FindGameObjectsWithTag("chest");

        foreach(GameObject chest in chests)
        {
            //La chest e' chiusa e che non sia quella che non si sta considerando che sia questa?
            if(!chest.GetComponent<Chest>().isOpen() && chest!=gameObject)
            {
                return;
            }
        }

        //Questa chest e' l'ultima ad essere aperta, il player non ha ancora trovato la chiave e quindi gli spawnera' la chiave.
        ratio_key = 1;
        ratio_vita = 0;
        ratio_bomba = 0;
        ratio_noOggetti = 0;
    }

    /// <summary>
    /// Spawn un oggetto della chest a caso.
    /// </summary>
    void SpawnOggetto()
    {
        UltimaChestEdit();

        int range_fascia = GeneraRangeValoriDiOgniOggetto();
        
        int valore = new System.Random().Next(range_fascia);

        string nome_oggetto = TrovaOggettoCorrispondente(valore);

        if (nome_oggetto.Equals(oggetti[0]))        //Spawna la chiave.
        {
            Instantiate(GameObject.FindGameObjectWithTag("key").gameObject,
                        transform.position + new Vector3(-0.011f, 0.051f, -0.1f),
                        transform.rotation);
        }
        else if(nome_oggetto.Equals(oggetti[1]))    //Spawna la vita.
        {
            Instantiate(GameObject.FindGameObjectWithTag("vita").gameObject,
                        transform.position + new Vector3(0.003f, 0.09f, -0.1f),
                        transform.rotation);
        }
        else if(nome_oggetto.Equals(oggetti[2]))    //Spawna la bomba.
        {
            GameObject bomba = Instantiate(GameObject.FindGameObjectWithTag("bomba").gameObject,
                                           transform.position + new Vector3(0.0f, 0.09f, -0.1f),
                                           transform.rotation);
            bomba.GetComponent<Bomba>().attivaBomba();
        }

        //print(valore + "\n" + nome_oggetto);
    }

    /// <summary>
    /// Per ogni oggetto viene creato un range di valori per cui e' spawnabile.
    /// </summary>
    /// <returns>Restituisce il valore m tale che il range e' tra 0 e m-1 costruito.</returns>
    private int GeneraRangeValoriDiOgniOggetto()
    {
        int valore_range = 0;
        Player p = player.GetComponent<Player>();
        oggetto2valori = new Dictionary<string, List<int>>();

        //Range valori di spawn della chiave.
        valore_range = AggiungiOggettoERangeValoriDiSpawn(oggetti[0], valore_range, ratio_key, p.HaveKey());

        //Range valori di spawn della vita.
        valore_range = AggiungiOggettoERangeValoriDiSpawn(oggetti[1], valore_range, ratio_vita, p.vita == p.VitaMax());

        //Range valori di spawn della bomba.
        valore_range = AggiungiOggettoERangeValoriDiSpawn(oggetti[2], valore_range, ratio_bomba, false);

        //Range valori di spawn di nessun oggetto.
        valore_range = AggiungiOggettoERangeValoriDiSpawn(oggetti[3], valore_range, ratio_noOggetti, false);

        return valore_range;
    }

    /// <summary>
    /// Aggiunge l'oggetto e il suo relativo range di valori di spawn.
    /// </summary>
    /// <param name="nome_oggetto">Nome dell'oggetto.</param>
    /// <param name="valore_range_sin">Estremo sinistro del range di valori di spawn.</param>
    /// <param name="ratio_oggetto">Ratio di spawn dell'oggetto.</param>
    /// <param name="valore_proprieta">Valore della proprieta' che permette all'oggetto (se e' true) 
    ///                                 di non avere un range di valori di spawn o no (se e' false).</param>
    /// <returns>Estremo sinistro del range di valori di spawn del prossimo oggetto.</returns>
    private int AggiungiOggettoERangeValoriDiSpawn(string nome_oggetto, int valore_range_sin, float ratio_oggetto, bool valore_proprieta)
    {
        List<int> range_valori;
        int perc_succ;

        if(valore_proprieta || ratio_oggetto==0)
        {
            range_valori = null;
            perc_succ = valore_range_sin;
        }
        else
        {
            range_valori = new List<int>();
            perc_succ = valore_range_sin + (int)(ratio_oggetto / ratio_tot * 100) + 1;

            range_valori.Add(valore_range_sin);
            range_valori.Add(perc_succ-1);
        }
        oggetto2valori.Add(nome_oggetto, range_valori);

        return perc_succ;
    }

    /// <summary>
    /// Dato un valore di spawn v, trova e restituisce il nome dell'oggetto per cui 
    /// tale valore v si trova nel suo range di valori di spawn.
    /// </summary>
    /// <param name="valore">Valore di spawn</param>
    /// <returns>Nome dell'oggetto per il quali si trova il valore v si trova nel suo range di valori di spawn.</returns>
    private string TrovaOggettoCorrispondente(int valore) 
    {
        List<int> range_valori;
        for (int i = 0; i < oggetti.Length; i++)
        {
            if (oggetto2valori.TryGetValue(oggetti[i], out range_valori))
            {
                //Valore di spawn si trova nel range di valori spawn dell'oggetto attuale?
                if ((range_valori != null) && (valore >= range_valori[0] && valore <= range_valori[1]))
                {
                    return oggetti[i];
                }
            }
        }

        return oggetti[3];
    }
}
