﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba : MonoBehaviour
{
    private const float DURATA_ANIMAZIONE_BOMBA = 0.5f;                 //Durata dell'animazione della bomba.
    private bool isActiveted = false;                                   //Indica se la bomba e' stata attivata o meno.
    private float tempoAnim;                                            //Contatore del tempo dell'animazione.

    // Start is called before the first frame update
    void Start()
    {
        tempoAnim = 0.0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(isActiveted)
        {
            tempoAnim += Time.fixedDeltaTime;

            if(tempoAnim >= DURATA_ANIMAZIONE_BOMBA)
            {
                GameObject esplosione = Instantiate(GameObject.FindGameObjectWithTag("explosion").gameObject,
                                                    transform.position,
                                                    transform.rotation);
                esplosione.GetComponent<Explosion>().destroy = true;

                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        colpiscePlayer(collision.gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        colpiscePlayer(collision.gameObject);
    }

    /// <summary>
    /// Attiva la bomba.
    /// </summary>
    public void attivaBomba()
    {
        isActiveted = true;
    }

    //Quando la bomba esplode, il player viene colpito solo se si trova vicino.
    void colpiscePlayer(GameObject o)
    {
        if (o.tag.Equals("player") && tempoAnim >= DURATA_ANIMAZIONE_BOMBA-0.02f)
        {
            o.GetComponent<Player>().HurtPlayerByBomb(transform.position);
        }
    }
}
