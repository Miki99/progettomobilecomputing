﻿using UnityEngine;

public class PiranhaPlant : Enemy
{
    private const float DISTANZA_MAX_SHOT_X = 1.2f;             //Range di distanza su asse x tra player e piranha plant per effettuare lo shoot.
    private const float DISTANZA_MAX_SHOT_Y = 1.0f;             //Range di distanza su asse y tra player e piranha plant per effettuare lo shoot.
    private const float DISTANZA_MAX_HURT_X = 0.5f;             //Range di distanza du asse x tra player e piranha plant per effettuare l'hurt.
    private const float DISTANZA_MAX_HURT_Y = 0.6f;             //Range di distanza du asse y tra player e piranha plant per effettuare l'hurt.
    private const float ISTANTE_FINE_STATO_SHOOT = 3.0f;        //Istante di tempo che finisce lo stato shoot del piranha plant.
    private const float DURATA_ANIMAZIONE_SHOOT = 0.75f;        //Durata di tempo dell'animazione shoot del piranha plant.
    private const float DURATA_ANIMAZIONE_HURT = 0.667f;        //Durata di tempo dell'animazione hurt del piranha plant.

    private Animator piranhaPlantAnimator;                      //Animator di piranha plant.
    private bool isShooting;                                    //Indica se e' attiavo o meno l'animazione dello sparo del piranha plant.
    private bool isShoot;                                       //Indica se ha sparato o meno il piranha plant.
    private bool isHurting;                                     //Indica se deve colpire o meno il player.
    private float tempoAnimShoot;                               //Tempo di esecuzione dell'animazione shoot.
    private float tempoAnimHurt;                                //Tempo di esecuzione dell'animazione hurt.
    
    // Start is called before the first frame update
    void Start()
    {
        piranhaPlantAnimator = GetComponent<Animator>();
        punteggioNemico = 20;
        isShooting = false;
        isShoot = false;
        isHurting = false;
        tempoAnimShoot = 0.0f;
        tempoAnimHurt = 0.0f;
        
        player = GameObject.FindGameObjectWithTag("player").gameObject;
        levelManager = GameObject.FindGameObjectWithTag("level_manager").gameObject;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CalcolaDistanzaXeY();
        //print("Player vita = " + player.GetComponent<Player>().vita);

        GuardaVersoIlPlayer();
        Shoot();
        Hurt();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("player"))
        {
            float angolo = Angolo();

            //Il piranha plant e' stato saltato sopra dal player?
            if ((angolo >= 50) && (angolo <= 140))
            {
                DeathByJumpedTopPlayer();
            }
            //Il piranha plant sta per colpire il player?
            else if ((tempoAnimHurt >= 5.0f / 8 * DURATA_ANIMAZIONE_HURT) && (6.0f / 8 * tempoAnimHurt <= DURATA_ANIMAZIONE_HURT) &&
                isHurting && !player.GetComponent<Player>().hurting)
            {
                ColpisciPlayer();
            }
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("player"))
        {
            float angolo = Angolo();

            //Il piranha plant sta per colpire il player?
            if ((tempoAnimHurt >= 5.0f / 8 * DURATA_ANIMAZIONE_HURT) && (6.0f / 8 * tempoAnimHurt <= DURATA_ANIMAZIONE_HURT) &&
                isHurting && !player.GetComponent<Player>().hurting)
            {
                ColpisciPlayer();
            }
        }
    }

    void GuardaVersoIlPlayer()
    {
        if(getDistanza().x < 0)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }
    }

    //Il piranha plant spara quando il player si trova in un certo range di distanza consentito.
    void Shoot()
    {
        //piranha plant spara se player si trova nel range consentito.
        /* La condizione del if e' spiegata nel seguente modo (piu' eusastuvo possibile):
         * il valore di deltaX e di delta.y devono essere, rispettivamente, |deltaX|<=DISTANZA_MAX_SHOT_X e 
         * |delta.y|<=DISTANZA_MAX_SHOT_Y, questi mi indicano il range quando sparera'; poi, il piranha plant rivolto 
         * verso sinistra viene indentificato con valore transform.localScale.x e che e' positivo, invece, 
         * se e' rivolto verso destra ga valore negativo; per ultimo sara' indicato il verso dello sparo, cioe'
         * per lo sparo viene accompagnato con dove e' rivolto (sinistra o destra) 
         * e sparera' solo nella stessa direzione, quindi se e' rivolto verso sinistra sparera' 
         * solo verso sinistra, invece, verso destra se e' rivolto verso destra. */
        if (((Mathf.Abs(getDistanza().x) <= DISTANZA_MAX_SHOT_X) && (Mathf.Abs(getDistanza().y) <= DISTANZA_MAX_SHOT_Y)) &&
            (IsRivoltoVersoSinistraEPlayerASinistra() || IsRivoltoVersoDestraEPlayerADestra()) && !isHurting)
            {
            //Spara se non ha sparato.
            if(!isShooting)
            {
                piranhaPlantAnimator.SetBool("IsShoot", true);
                isShooting = true;
                isShoot = false;
            }

            tempoAnimShoot += Time.fixedDeltaTime;

            //Shoot dei ball di piranha plant.
            if((tempoAnimShoot >= 6.0f/9 * DURATA_ANIMAZIONE_SHOOT) && !isShoot)
            {
                isShoot = true;
                ShootBall();
            }

            //Fa terminare l'animazione dello shoot.
            if(tempoAnimShoot >= DURATA_ANIMAZIONE_SHOOT && piranhaPlantAnimator.GetBool("IsShoot"))
            {
                piranhaPlantAnimator.SetBool("IsShoot", false);
            }

            //Ricomincia lo stato di shoot.
            if(tempoAnimShoot >= ISTANTE_FINE_STATO_SHOOT)
            {
                tempoAnimShoot = 0.0f;
                isShooting = false;
                isShoot = false;
            }
        }
        //Il piranha plant si trova nello stato di hurt?
        else if(isHurting)
        {
            piranhaPlantAnimator.SetBool("IsShoot", false);
            isShooting = true;
            isShoot = true;
            tempoAnimShoot = 0.75f * ISTANTE_FINE_STATO_SHOOT;
        }
        else
        {
            piranhaPlantAnimator.SetBool("IsShoot", false);
            isShooting = false;
            isShoot = false;
            tempoAnimShoot = 0.0f;
        }
    }

    //Il piranha plant spara la sua ball.
    void ShootBall()
    {
        //Spara la ball verso sinistra se il player se il piranha plant e' rivolto verso sinistra e che il player si trova nel suo verso. 
        if(IsRivoltoVersoSinistraEPlayerASinistra())
        {
            GameObject ppBallClone = Instantiate(GameObject.FindGameObjectWithTag("piranha_plant_ball").gameObject, 
                                                 transform.position + new Vector3(-0.15f, 0f, 0f), transform.rotation);
            ppBallClone.GetComponent<PiranhaPlantBall>().sparaVersoSinistra = true;
        }

        //Spara la ball verso destra se il player se il piranha plant e' rivolto verso destra e che il player si trova nel suo verso. 
        if (IsRivoltoVersoDestraEPlayerADestra())
        {
            GameObject ppBallClone = Instantiate(GameObject.FindGameObjectWithTag("piranha_plant_ball").gameObject,
                                                 transform.position + new Vector3(0.15f, 0f, 0f), transform.rotation);
            ppBallClone.GetComponent<PiranhaPlantBall>().sparaVersoDestra = true;
        }  
    }

    //Il piranha plant colpisce il player.
    void Hurt()
    {
        //Il piranha plant e' nella distanza tale da poter farsi colpire il player e che si trovi di fronte a se'?
        if ((Mathf.Abs(getDistanza().x) <= DISTANZA_MAX_HURT_X) && (Mathf.Abs(getDistanza().y) <= DISTANZA_MAX_HURT_Y) && 
            (IsRivoltoVersoSinistraEPlayerASinistra() || IsRivoltoVersoDestraEPlayerADestra()))
        {
            if(!piranhaPlantAnimator.GetBool("IsHurt"))
            {
                piranhaPlantAnimator.SetBool("IsHurt", true);
                isHurting = true;
            }

            tempoAnimHurt += Time.fixedDeltaTime;

            if(IsRivoltoVersoSinistraEPlayerASinistra())
            {
                transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }
            if(IsRivoltoVersoDestraEPlayerADestra())
            {
                transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            }

            if(tempoAnimHurt >= DURATA_ANIMAZIONE_HURT)
            {
                piranhaPlantAnimator.SetBool("IsHurt", false);
                tempoAnimHurt = 0.0f;
            }
        }
        else
        {
            piranhaPlantAnimator.SetBool("IsHurt", false);
            isHurting = false;
            tempoAnimHurt = 0.0f;
        }
    }

    //true se il piranha plant e' rivolto verso sinistra e che player si trova alla sinistra del piranha plant, false altrimenti.
    private bool IsRivoltoVersoSinistraEPlayerASinistra()
    {
        return ((transform.localScale.x > 0) && (getDistanza().x < 0));
    }

    //true se il piranha plant e' rivolto verso destra e che player si trova alla destra del piranha plant, false altrimenti.
    private bool IsRivoltoVersoDestraEPlayerADestra()
    {
        return ((transform.localScale.x < 0) && (getDistanza().x > 0));
    }

    public override void ColpisciPlayer(Vector2 velocitaHurtPlayer)
    {
        //Fa volare il player nel verso in cui ha lo stesso del piranha plant.
        if (IsRivoltoVersoSinistraEPlayerASinistra())
        {
            player.GetComponent<Player>().PlayerHurted(new Vector2(-velocitaHurtPlayer.x, velocitaHurtPlayer.y));
        }
        if (IsRivoltoVersoDestraEPlayerADestra())
        {
            player.GetComponent<Player>().PlayerHurted(velocitaHurtPlayer);
        }
    }
}
