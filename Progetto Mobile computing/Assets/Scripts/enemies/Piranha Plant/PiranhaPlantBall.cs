﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiranhaPlantBall : MonoBehaviour
{
    private const float VELOCITA_PLAYER_HURT_X = 1.5f;          //Velocita' sull'asse x di spinta quando il player viene colpito.
    private const float VELOCITA_PLAYER_HURT_Y = 2.0f;          //Velocita' sull'asse y di spinta quando il player viene colpito.
    private const float SPEED = 0.825f;                         //Velocita' della ball di piranha plant.
    private Rigidbody2D piranhaPlantBallRB;                     //Rigid body della ball di piranha plant.

    /// <summary>
    /// Indica se si deve muovere verso destra.
    /// </summary>
    public bool sparaVersoDestra { set; get; }

    /// <summary>
    /// Indica se si deve muovere verso sinistra.
    /// </summary>
    public bool sparaVersoSinistra { set; get; }

    private void Start()
    {
        piranhaPlantBallRB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //E' stato indicato che la ball si deve muovere verso destra?
        if(sparaVersoDestra)
        {
            transform.localScale = new Vector3(-1, 1, 1);
            piranhaPlantBallRB.velocity = new Vector2(SPEED, 0);
        }

        //E' stato indicato che la ball si deve muovere verso sinistra?
        if (sparaVersoSinistra)
        {
            piranhaPlantBallRB.velocity = new Vector2(-SPEED, 0);
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        //Ha colpito il player?.
        if (collision.gameObject.tag.Equals("player"))
        {
            //il player perde una vita e viene spinto in un'altra direzione.
            GameObject player = collision.gameObject;
            Vector2 distanza = player.transform.position - transform.position;
            float angolo = Mathf.Atan2(distanza.y, distanza.x) * Mathf.Rad2Deg;

            //Il player si trova alla sinistra della ball e che lo si trova di fronte?
            if ((distanza.x < 0) && (transform.localScale.x > 0) && ((angolo >= 120) || (angolo <= -120)))
            {
                player.GetComponent<Player>().PlayerHurted(new Vector2(-VELOCITA_PLAYER_HURT_X, VELOCITA_PLAYER_HURT_Y));
            }
            //Il player si trova alla destra della ball e che lo si trova di fronte?
            else if ((distanza.x > 0) && (transform.localScale.x < 0) && ((angolo <= 60) || (angolo >= -60)))
            {
                player.GetComponent<Player>().PlayerHurted(new Vector2(VELOCITA_PLAYER_HURT_X, VELOCITA_PLAYER_HURT_Y));
            }
            else
            {
                player.GetComponent<Player>().PlayerHurted(new Vector2(0.0f, VELOCITA_PLAYER_HURT_Y));
            }

            Destroy(gameObject);
        }
        //Collide con il terreno?
        else if(collision.gameObject.tag.Equals("terreno"))
        {
            //La ball si distrugge una volta colliso con il terreno.
            Destroy(gameObject);
        }
    }
}
