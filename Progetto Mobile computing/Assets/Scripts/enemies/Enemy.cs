﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    protected int punteggioNemico;              //Punteggio del nemico quando viene ucciso dal player.

    protected GameObject player;                //Game Object di player.
    protected GameObject levelManager;          //Game Object del level manager.
    private Vector2 distanza;                   //Distanza tra il nemico e il player per i rispettivi assi.

    /// <summary>
    /// Si calcola la distanza tra player e il nemico per i rispettivi assi cartesiani x e y.
    /// </summary>
    public void CalcolaDistanzaXeY()
    {
        distanza = player.transform.position - transform.position;
    }

    /// <summary>
    /// Restituisce il vettore della distanza tra player e nemico dei rispettivi assi cartesiani x e y.
    /// Puo' essere interpretato anche in un altro modo i valori del vettore. Lo si puo' intepretare
    /// i valori assunti del vettore come la posizione del piano cartesiano dove all'origine e' la posizione
    /// del nemico e i valori del vettore la posizione del player nel piano cartesiano, esplicitato
    /// nel seguente modo: <br></br>
    ///         y 
    ///         ^ 
    ///         | 
    ///         |   . P 
    ///         | 
    /// --------+--------> x 
    ///         | 
    ///         | 
    ///         | <br></br>
    /// dove l'origine e' la posizione del nemico e i valori del vettore distanza indica
    /// la posizione del player del punto P nel piano cartesiano.
    /// </summary>
    /// <returns>Vettore della distanza tra player e nemico dei rispettivi assi cartesiani x e y.</returns>
    public Vector2 getDistanza()
    {
        return distanza;
    }

    /// <summary>
    /// Distanza tra tra player e il nemico.
    /// </summary>
    /// <returns>Valore di distanza tra il player e il nemico. </returns>
    public float ValoreDistanza()
    {
        return Mathf.Sqrt(distanza.x * distanza.x + distanza.y * distanza.y);
    }

    /// <summary>
    /// Calcola l'angolo che si ottiene da posizione P del player e l'origine (posizione del nemico).
    /// </summary>
    /// <returns>Angolo (in gradi) di distanza calcolata tra player e nemico.</returns>
    public float Angolo()
    {
        float angolo = Mathf.Atan2(distanza.y, distanza.x) * Mathf.Rad2Deg;
        if (angolo < 0)     angolo = 180 - angolo;
        return angolo;
    }

    /// <summary>
    /// Quando il nemico colpisce il player, li fa perdere una vita e lo spinge in un'altra direzione opposta
    /// della direzione che il nemico ha colpito.
    /// </summary>
    /// <param name="velocitaHurtPlayer">Velocita hurt del player</param>
    public virtual void ColpisciPlayer(Vector2 velocitaHurtPlayer)
    {
        //Il player si trova alla sinistra del nemico?
        if (distanza.x < 0)
        {
            player.GetComponent<Player>().PlayerHurted(new Vector2(-velocitaHurtPlayer.x, velocitaHurtPlayer.y));
        }
        else
        {
            player.GetComponent<Player>().PlayerHurted(velocitaHurtPlayer);
        }
    }

    /// <summary>
    /// Quando il nemico colpisce il player, li fa perdere una vita e lo spinge in un'altra direzione opposta
    /// della direzione che il nemico ha colpito con una velocita' prestabilita.
    /// </summary>
    public virtual void ColpisciPlayer()
    {
        ColpisciPlayer(new Vector2(Player.VELOCITA_X_PLAYER_HURTED, Player.VELOCITA_Y_PLAYER_HURTED));
    }

    /// <summary>
    /// Fa morire il nemico quando si verifica un determinato evento che lo scaturisce.
    /// </summary>
    public void Death()
    {
        GameObject enemyDeath = Instantiate(GameObject.FindGameObjectWithTag("explosion"), transform.position, transform.rotation);
        enemyDeath.GetComponent<Explosion>().destroy = true;

        Destroy(gameObject);
    }

    /// <summary>
    /// Il nemico viene ucciso dal player.
    /// </summary>
    public void DeathByPlayer()
    {
        levelManager.GetComponent<LevelManager>().analizzaOggettoEAggiornaStatistiche(gameObject);
        Death();
    }

    /// <summary>
    /// Fa morire il nemico quando il player li salta sopra.
    /// </summary>
    public void DeathByJumpedTopPlayer()
    {
        player.GetComponent<Player>().Jump();
        DeathByPlayer();
    }

    /// <summary>
    /// Restituisce il punteggio del nemico ucciso dal player.
    /// </summary>
    /// <returns>Punteggio del nemico ucciso.</returns>
    public int GetPunteggio()
    {
        return punteggioNemico;
    }
}
