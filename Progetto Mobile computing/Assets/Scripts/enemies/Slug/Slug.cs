﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slug : Enemy
{
    private const float SPEED_MOVE = 0.5f;                      //Velocita' di movimento della lumaca.
    private Rigidbody2D slugRG;                                 //Rigid body della lumaca.
    private Vector2 posizioneInizialeSlug;                      //Posizione iniziale della lumaca.
    public float spostamentoX = 1.5f;                           //Di quanto si vuole spostare dalla posizione iniziale.

    // Start is called before the first frame update
    void Start()
    {
        slugRG = GetComponent<Rigidbody2D>();
        posizioneInizialeSlug = new Vector2(transform.position.x, transform.position.y);
        punteggioNemico = 20;

        player = GameObject.FindGameObjectWithTag("player").gameObject;
        levelManager = GameObject.FindGameObjectWithTag("level_manager").gameObject;

        slugRG.velocity = new Vector2(-SPEED_MOVE, 0.0f);
        transform.localScale = new Vector3(1, 1, 1);
    }

    void FixedUpdate()
    {
        Move();
    }

    //Movimento della lumaca.
    void Move()
    {
        if(posizioneInizialeSlug.x - transform.position.x >= spostamentoX)
        {
            slugRG.velocity = new Vector2(SPEED_MOVE, 0);
            transform.localScale = new Vector3(-1, 1, 1);
        }

        if(posizioneInizialeSlug.x - transform.position.x <= -spostamentoX)
        {
            slugRG.velocity = new Vector2(-SPEED_MOVE, 0);
            transform.localScale = new Vector3(1, 1, 1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("player"))
        {
            //Il player ha saltato sora alla lumaca?
            CalcolaDistanzaXeY();
            float angolo = Angolo();

            if ((angolo >= 35) && (angolo <= 145))
            {
                DeathByJumpedTopPlayer();
            }
            //La lumaca colpisce il player.
            else
            {
                ColpisciPlayer();
            }
        }
    }
}
