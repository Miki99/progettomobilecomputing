﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : Enemy
{
    private const float DISTANZA_X_MAX_PLAYER = 1.5f;           //Range di distanza su asse x tra la rana e il player per far muovere la rana per inseguire il player.
    private const float DISTANZA_Y_MAX_PLAYER = 0.75f;          //Range di distanza su asse y tra la rana e il player per far muovere la rana per inseguire il player.

    private const float DURATA_STATO_MOVE = 1.5f;               //Istante di tempo in cui termina lo stato di move della rana.
    private const float SPEED_MOVE = 1.0f;                      //Velocita' di movemento della rana.
    private const float SPEED_JUMP = 2.31f;                     //Velocita' di salto della rana.
    private Animator frogAnimator;                              //Animator della rana.
    private Rigidbody2D frogRG;                                 //Rigid Body della rana.
    private CircleCollider2D frogCC;                            //Circle Collider della rana.
    private float frogCCRadius;                                 //Raggio del Cicle Collider della rana.
    private float tempoStatoMove;                               //Tempo di esecuzione dello stato di move della rana.
    private bool isMoving;                                      //Indica se la rana e' ferma o e' in movimento.

    // Start is called before the first frame update
    void Start()
    {
        frogAnimator = GetComponent<Animator>();
        frogRG = GetComponent<Rigidbody2D>();
        frogCC = GetComponent<CircleCollider2D>();
        tempoStatoMove = 0.0f;
        isMoving = false;
        frogCCRadius = frogCC.radius;
        punteggioNemico = 20;

        player = GameObject.FindGameObjectWithTag("player").gameObject;
        levelManager = GameObject.FindGameObjectWithTag("level_manager").gameObject;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CalcolaDistanzaXeY();

        Move();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //Il player collide la rana?
        if (collision.collider.tag.Equals("player"))
        {
            float angolo = Angolo();
            //print("Angolo = " + angolo);

            //Il player ha saltato sopra la rana?
            if (((angolo >= 45) && (angolo <= 135) && (!isMoving || frogAnimator.GetBool("IsFalling"))) ||
                ((((angolo >= 45) && (angolo <= 115)) || ((angolo >= 65 && angolo <= 135))) && isMoving))
            {
                DeathByJumpedTopPlayer();
            }
            //Il player perde una vita e viene spinto in un'altra direzione.
            else
            {
                ColpisciPlayer();
            }
        }
    }

    //Fa muovere la rana solo qunado il player si trova in certa distanza che li consente di seguirlo.
    void Move()
    {
        Vector2 distanza = getDistanza();

        //Il player di trova nel range di distanza prestabilito che permette di far muovere la rana?
        if ((Mathf.Abs(distanza.x) < DISTANZA_X_MAX_PLAYER) && (Mathf.Abs(distanza.y) < DISTANZA_Y_MAX_PLAYER))
        {
            //La rana si muove se sta ferma.
            if (!isMoving)
            {
                frogAnimator.SetBool("IsMoving", true);
                isMoving = true;
                frogCC.radius = 0.15f;

                //La rana si muove verso sinistra se il player si trova alla sua sinista, va verso destra altrimenti.
                if (distanza.x < 0)
                {
                    frogRG.velocity = new Vector2(-SPEED_MOVE, SPEED_JUMP);
                    transform.localScale = new Vector3(1, 1, 1);
                }
                else
                {
                    frogRG.velocity = new Vector2(SPEED_MOVE, SPEED_JUMP);
                    transform.localScale = new Vector3(-1, 1, 1);
                }  
            }

            MovementAnimations();
        }
        //La rana si trova fuori dal range di distanza tra se stessa e il player e continua a muoversi fino a quando non tocca il terreno.
        else if(isMoving)
        {
            MovementAnimations();
        }
        //La rana si trova distante dal player.
        else
        {
            frogAnimator.SetBool("IsMoving", false);
            frogAnimator.SetBool("IsFalling", false);
            isMoving = false;
            tempoStatoMove = 0.0f;
            frogCC.radius = frogCCRadius;
        }
    }

    void MovementAnimations()
    {
        //Imposta l'animazione di caduta della rana quando e' in movimento.
        if (isMoving)
            frogAnimator.SetBool("IsFalling", (frogRG.velocity.y < 0));

        //La rana ha "colliso" (ha toccato) il terreno
        if (frogRG.velocity.y == 0)
        {
            frogAnimator.SetBool("IsMoving", false);
            frogAnimator.SetBool("IsFalling", false);
            frogCC.radius = frogCCRadius;
        }

        tempoStatoMove += Time.fixedDeltaTime;

        //Ricominci lo stato di move della rana.
        if (tempoStatoMove >= DURATA_STATO_MOVE)
        {
            isMoving = false;
            tempoStatoMove = 0.0f;
        }
    }
}
