﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eagle : Enemy
{
    private const float DISTANZA_MAX_X = 1.5f;                      //Range di distanza (sull'asse x) tra l'aquila e player massima per andare verso il player e attaccare.                      
    private const float DISTANZA_MAX_Y = 1.5f;                      //Range di distanza (sull'asse y) tra l'aquila e player massima per andare verso il player e attaccare.
    private const float DURATA_ANIMAZIONE_ATTACK = 0.583f;          //Durata di tempo dell'animazione dell'aquila.
    private const float DURATA_STATO_ATTACK = 1.166f;               //Durata dell stato di Attack dell'aquila;
    private const float SPEED_MOVE_IDLE = 0.11f;                    //Velocita' di movimento dell'aquila dell'aquila quando sta solo volando.
    private const float SPEED_MOVE_ATTACK = 0.25f;                  //Velocita' di movimento dell'aquila quando deve attaccare.
    private const float DELTA_SPOSTAMENTO_IDLE = 0.25f;             //Indica lo spostamento dell'aquila quando vola.

    private Rigidbody2D eagleRG;                                    //Rigid body dell'aquila.
    private Animator eagleAnimator;                                 //Animator dell'aquila
    private bool isMoving;                                          //Indica se l'aquila si sta muovendo o meno per raggiungere il player e attccare.
    private bool isAttacking;                                       //Indica se l'aquila sta attacando o meno.
    private bool isIdle;                                            //Indica o meno se l'aquila e' fermo (vola soltanto).
    private bool isShoot;                                           //Indica se l'aquila ha sparato o meno.
    private float tempoAnimAttack;                                  //Tempo dell'animazione attack (in corso). 
    private Vector2 eaglePosizioneIniziale;                         //Posizione iniziale dell'aquila.

    // Start is called before the first frame update
    void Start()
    {
        eagleRG = GetComponent<Rigidbody2D>();
        eaglePosizioneIniziale = new Vector2(transform.position.x, transform.position.y);
        eagleAnimator = GetComponent<Animator>();
        punteggioNemico = 50;
        tempoAnimAttack = 0.0f;
        isAttacking = false;
        isMoving = false;
        isIdle = false;
        isShoot = false;

        player = GameObject.FindGameObjectWithTag("player").gameObject;
        levelManager = GameObject.FindGameObjectWithTag("level_manager").gameObject;

        eagleRG.velocity = new Vector2(0.0f, SPEED_MOVE_IDLE);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CalcolaDistanzaXeY();
        GuardaVersoIlPlayer();

        if (!isIdle && !isMoving && !isAttacking)
        {
            isIdle = true;
            eagleRG.velocity = new Vector2(0.0f, SPEED_MOVE_IDLE);
        }

        if (!isMoving && !isAttacking)
            Idle();

        if(!isAttacking)
            Move();

        Attack();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //L'aquila collide il player?
        if (collision.collider.tag.Equals("player"))
        {
            float angolo = Angolo();

            if(angolo>=0 && angolo<=180)
            {
                DeathByJumpedTopPlayer();
            }
            else
            {
                DeathByPlayer();
            }
        }
    }

    /// <summary>
    /// L'aquila si rivolge nella direzione del player.
    /// </summary>
    void GuardaVersoIlPlayer()
    {
        if (getDistanza().x < 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    //L'aquila vola quando non fa nulla.
    void Idle()
    {
        if(transform.position.y - eaglePosizioneIniziale.y >= DELTA_SPOSTAMENTO_IDLE)
        {
            eagleRG.velocity = new Vector2(0.0f, -SPEED_MOVE_IDLE);
        }
        else if(transform.position.y - eaglePosizioneIniziale.y <= 0)
        {
            eagleRG.velocity = new Vector2(0.0f, SPEED_MOVE_IDLE);
        }
    }

    //L'aquila si muove per raggiungere il player.
    void Move()
    {
        Vector2 distanza = getDistanza();

        //Il player si trova nel range consentito per far muovere (farsi inseguire) l'aquila e non si sta muovendo l'aquila?
        if ((Mathf.Abs(distanza.x) <= DISTANZA_MAX_X) && ((distanza.y < 0) && (distanza.y >= -DISTANZA_MAX_Y)) && !isMoving)
        {
            isIdle = false;
            isMoving = true;

            //Si dirige verso il player.
            if (distanza.x < 0)
            {
                eagleRG.velocity = new Vector2(-SPEED_MOVE_ATTACK, 0.0f);
                //transform.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                eagleRG.velocity = new Vector2(SPEED_MOVE_ATTACK, 0.0f);
                //transform.localScale = new Vector3(-1, 1, 1);
            }
        }
        else
        {
            isMoving = false;
        }
    }

    //L'aquila attacca al player quando si trova nel range consentito.
    void Attack()
    {
        Vector2 distanza = getDistanza();
        float angolo = Angolo();

        if((Mathf.Abs(distanza.x) <= DISTANZA_MAX_X) && ((distanza.y < 0) && (distanza.y >= -DISTANZA_MAX_Y)) &&
            (angolo >= 215) && (angolo <= 315)) 
        {
            //L'aquila attacca se non l'ha gia' fatto.
            if(!isAttacking)
            {
                eagleAnimator.SetBool("Attack", true);
                isAttacking = true;

                eagleRG.velocity = new Vector2(0.0f, 0.0f);
                isIdle = false;
                isMoving = false;
            }

            tempoAnimAttack += Time.fixedDeltaTime;

            //L'aquila spara.
            if((tempoAnimAttack >= 5.0f/7 * DURATA_ANIMAZIONE_ATTACK) && !isShoot)
            {
                ShootBall();
                isShoot = true;
                //print("Ho sparato!");
            }

            //Termina l'animazione attack dell'aquila.
            if((tempoAnimAttack >= DURATA_ANIMAZIONE_ATTACK) && eagleAnimator.GetBool("Attack"))
            {
                eagleAnimator.SetBool("Attack", false);
            }

            //Termina lo stato attack della' aquila.
            if(tempoAnimAttack >= DURATA_STATO_ATTACK)
            {
                isAttacking = false;
                isShoot = false;
                tempoAnimAttack = 0.0f; 
            }
        }
        else
        {
            eagleAnimator.SetBool("Attack", false);
            isAttacking = false;
            isShoot = false;
            tempoAnimAttack = 0.0f;
        }
    }

    //Si calcola la velocita' iniziale per far in modo di colpire player in modo preciso.
    Vector2 CalcolaVelocitaInizialeBall()
    {
        Vector2 v_player = player.GetComponent<Rigidbody2D>().velocity;         //Velocita' del player per quell'istante.
        float distanza_y = Mathf.Abs(getDistanza().y) /*+ 0.108f*/;             //Distanza y tra player e ball.
        float distanza_x = Mathf.Abs(getDistanza().x) - 0.16f;                  //Distanza x tra player e ball.
        float gravita = Mathf.Abs(Physics2D.gravity.y);                         //Valore della gravità.
        float distanza, tempo;
        float velocitaX_ball;                                                   //Velocità x iniziale della ball.

        /* Qui a seguire si fa una spiegazione schematica di come si trova il modo di calcolare la velocita' iniziale della ball.
         * Si e' impostato il problema per la cinematica del punto materiale per trovare la velocità iniziale.
         * - CASO IN CUI IL PLAYER SI TROVA SUL TERRENO (il player si trova alla sinistra della ball):
         *         ^ y
         *         |
         *       h +    .2
         *         |
         * --------.---+----> x
         *         |1  d_12
         *         |
         *         |
         *         
         * 1 e' il player e si trova nell'origine, 2 e' la ball dell'aquila.
         * Equazioni del moto del player e della  ball:
         * x1 = v_01x * t
         * y1 = 0
         * x2 = - v_02x * t + d_12
         * y2 = - 1/2 * g * t^2 + h
         * 
         * Legenda:
         * v_01x = v_player.x
         * v_02x = velocitaX_ball
         * d_12 = distanza_x
         * h = distanza_y
         * 
         * Si deve prevedere la posizione dove colpire il player e di conseguenza ottenere il valore v_02x si deve mettere a sistema
         * le equazioni di moto del player e della ball e si otterra' che 
         * v_02x = -v_01x + d12 / t
         * t = sqrt(2 * h / g)
         * 
         * Nel caso in cui il player si trova alla destra del player della ball cambia solo i segni nell'equazione di x1
         * e nel piano cartesiano si trova alla sinistra del player (non alla destra come strutturato 
         * che il player si trova alla sinistra della ball). Il valore di v_02x sara' analogo a quello trovato in precedenza,
         * cambia solo per i segni.
         * 
         * - CASO IN CUI IL PLAYER NON SI TROVA SUL TERRENO, MA IN ARIA (il player si trova alla sinistra della ball):
         *                y
         *                ^
         *                |
         *             h_2+         .2
         *                |
         *             h_1+  .1
         * ---------------+--+------+-----> x
         *                | d_1    d_2
         *                |
         *                |
         *                |
         *                
         * 1 e' il player, 2 e' la ball dell'aquila.
         * Equazioni del moto del player e della  ball:
         * x1 = v_01x * t
         * y1 = - 1/2 * g * t^2 + v_01y * t
         * x2 = - v_02x * t + d_2
         * y2 = - 1/2 * g * t^2 + h_2
         * 
         * v_1y = -g * t + v_01y
         * 
         * Legenda:
         * v_01x = v_player.x
         * v_01y = velocita' di salto del player (valore della variabile speedJump di Player)
         * v_1y = velocita' y del player per l'istante t
         * d_1 = distanza x tra origine e il player
         * h_1 = distanza_y tra origine e il player
         * v_02x = velocitaX_ball
         * d_2 = distanza x tra origine e la ball
         * h_2 = distanza_y tra origine e la ball
         * 
         * PREMESSA: le equazioni del player rappresentano il modo in cui avviene il suo moto e 
         *           nel piano cartesiano rappresenta un istante di tempo del moto delle equazioni.
         * 
         * Prima di tutto si deve conoscere d_1 e h_1 e per farlo conosco v_1y = v_player.y risolvendo
         * l'equazione v_1y = -g * t + v_01y e si ottiene che t = (v_01y - v_1y)/g
         * Conosco l'istante di tempo in cui ho la velocita' v_1y e per quell'istante di tempo coincide
         * alle posizioni di d_1 e h_1. Sostituendo l'istante di tempo nell'equazioni del player
         * ho i valori delle posizione:
         * h_1 = (v_01y^2 - v_1y^2)/g
         * d_1 = v_01x * (v_01y - v_1y) / g
         * 
         * Sapendo che d_12 = d_2 - d_1 e d_12 = distanza_x, h_12 = h_2 - h_1 e h_12 = distanza_y
         * ho che d_2 = distanza_x + d_1 e h_2 = distanza_y + h_1.
         * 
         * Si deve prevedere la posizione dove colpire il player e di conseguenza ottenere il valore v_02x, si deve mettere a sistema
         * le equazioni di moto del player e della ball e si otterra' che 
         * v_02x = -v_01x + d_2 / t
         * t = h_2 / v_01y
         * 
         * Nel caso in cui il player si trova alla destra della ball e' analogo a cio' che e' descritto prima, cambia solo
         * che cambia le posizione della ball e i segni sulle equazioni. */

        //Il player si trova sul terreno?
        if (player.GetComponent<Player>().IsGrounded())
        {
            float altezza = distanza_y;               //altezza = h = distanza_y
            distanza = distanza_x;                    //distanza = d_12 = distanza_x
            tempo = Mathf.Sqrt(2 * altezza / gravita);
        }
        else
        {
            float v_01y = player.GetComponent<Player>().SpeedJump();
            float h_1 = (Mathf.Pow(v_01y, 2) - Mathf.Pow(v_player.y, 2)) / gravita;
            float d_1 = v_player.x * (v_01y - v_player.y) / gravita;
            float h_2 = distanza_y + h_1;

            distanza = distanza_x + d_1;              //distanza = d_12 + d_1
            tempo = h_2 / v_01y;
        }

        velocitaX_ball = -v_player.x + distanza / tempo;

        if (getDistanza().x < 0)
            velocitaX_ball *= -1;

        if ((transform.localScale.x > 0 && velocitaX_ball > 0) || (transform.localScale.x < 0 && velocitaX_ball < 0))
            velocitaX_ball = 0.0f;

        return new Vector2(velocitaX_ball, 0f);
    }

    //Riduce la precisione dello shoot dell'aquila andando a modificare la velocita' iniziale della ball.
    Vector2 RiduciPrecisioneDelloShoot(Vector2 velocitaInizialeBall)
    {
        float vel_x = velocitaInizialeBall.x;

        if(Mathf.Abs(velocitaInizialeBall.x) <= 0.3f)
        {
            vel_x = 1.0f * ((float) new System.Random().Next(10)) / 9;

            if (getDistanza().x < 0)
                vel_x *= -1;
        }
        else
        {
            vel_x = velocitaInizialeBall.x * (1.0f + 0.40f * new System.Random().Next(2));
        }

        return new Vector2(vel_x, velocitaInizialeBall.y);
    }

    //L'aquila spara la ball verso il player.
    void ShootBall()
    {
        GameObject eagleBall;

        if (transform.localScale.x > 0)
        {
            eagleBall = Instantiate(GameObject.FindGameObjectWithTag("eagle_ball").gameObject, (transform.position + new Vector3(-0.16f, -0.0755f, 0.0f)), transform.rotation);
        }
        else
        {
            eagleBall = Instantiate(GameObject.FindGameObjectWithTag("eagle_ball").gameObject, (transform.position + new Vector3(0.16f, -0.0755f, 0.0f)), transform.rotation);
        }

        eagleBall.GetComponent<EagleBall>().SetVelocitaIniziale( RiduciPrecisioneDelloShoot( CalcolaVelocitaInizialeBall() ) );
        eagleBall.GetComponent<EagleBall>().SetMoveBall(true);
    }
}
