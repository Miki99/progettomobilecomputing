﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EagleBall : MonoBehaviour
{
    private const float VELOCITA_PLAYER_HURT_X = 1.5f;                  //Velocita' x di hurt del player.
    private const float VELOCITA_PLAYER_HURT_Y = 2.0f;                  //Velocita' y di hurt del player.
    Rigidbody2D eagleBallRB;                                            //Rigidbody di ball dell'aquila.
    private Vector2 speedIniziale = new Vector2();                      //Velocita' iniziale della ball quando viene sparata.
    private bool move = false;                                          //Inidica se la ball si muove o meno.
    private bool isShooted = false;                                     //Indica se la ball viene sparata o meno.

    // Start is called before the first frame update
    void Start()
    {
        eagleBallRB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(move && !isShooted)
        {
            eagleBallRB.bodyType = RigidbodyType2D.Dynamic;
            isShooted = true;
            eagleBallRB.velocity = speedIniziale;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //Ha colpito il player?
        if (collision.gameObject.tag.Equals("player"))
        {
            GameObject player = collision.gameObject;

            //Il player viene spinto nella direzione opposta di ball e perde una vita.
            if (player.transform.position.x < transform.position.x)
                player.GetComponent<Player>().PlayerHurted(new Vector2(-VELOCITA_PLAYER_HURT_X, VELOCITA_PLAYER_HURT_Y));
            else
                player.GetComponent<Player>().PlayerHurted(new Vector2(VELOCITA_PLAYER_HURT_X, VELOCITA_PLAYER_HURT_Y));

            Destroy(gameObject);
        }
        //Las ball si autodistrugge quando colpisce il terreno.
        if(collision.gameObject.tag.Equals("terreno"))
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Imposta se la ball si deve muovere o meno.
    /// </summary>
    /// <param name="move">true se si vuole far muovere la ball, false altrimenti.</param>
    public void SetMoveBall(bool move)
    {
        this.move = move;
    }

    /// <summary>
    /// Imposta la velocità iniziale della ball
    /// </summary>
    /// <param name="velocitaIniziale">Velocià iniziale della ball.</param>
    public void SetVelocitaIniziale(Vector2 velocitaIniziale)
    {
        speedIniziale = velocitaIniziale;
    }
}
