﻿using UnityEngine;
public class Player : MonoBehaviour
{
    public const float VELOCITA_X_PLAYER_HURTED = 1.25f;        //Velocita' su asse x di qunado il player e' colpito.
    public const float VELOCITA_Y_PLAYER_HURTED = 2.25f;        //Velocita' su asse y di qunado il player e' colpito.

    private const float DURATA_STATO_HURT = 0.5f;               //Durata (di tempo) dello stato di hurt di player.

    private Rigidbody2D playerRB;                               //Rigid body del player.
    private Animator playerAnimator;                            //Animator del player.
    CapsuleCollider2D playerCollider;                           //Capsule Collider 2D del player.

    private Comandi comandiPlayer;                              //Comandi per far muovere il player.

    private bool isGrounded = false;                            //Indica se il player e' poggiato o meno sul terreno.
    private bool haveKey = false;                               //Indica se il player ha raccolto o meno la chiave.
    private bool muoviSuScale = false;                          //Indica se il player si puo' muovere o meno sulle scale.
    private bool isSuScalaCima = false;                         //Indica se il player e' o meno in cima sulle scale.
    private bool sincronizzaColpoDaBomba = false;               //Indica se sincronizzare o meno il colpo da bomba per colpire il player.
    private float tempoAnimHurt = 0.0f;                         //Tempo dello stato di hurt.
    private float tempoAttesaPerColpoBomba = 0f;                //Tempo da attendere per sincronizzare quando sara' colpito dalla bomba. 
    private Vector3 posizioneIniziale;                          //Posizione iniziale del player all'inizio del livello.

    public int vita = 3;                                        //Vita del player.
    [SerializeField] int vitaMax = 5;                           //Vita max che puo' avere il player.
    [SerializeField] float speedMove = 1.0f;                    //Velocita' di movimento del player.
    [SerializeField] float speedJump = 3f;                      //Velocita' di salto del player.
    [SerializeField] AudioSource jumpSound;                     //Suono del salto.
    [SerializeField] AudioSource hurtedSound;                   //Suono di quando il player viene colpito.
    [SerializeField] AudioSource oggettoPickupSound;            //Suono dell'oggetto preso.

    Vector2 sizeColliderOriginale;                              //Size originale del collider del player.
    Vector2 offesetColliderOriginale;                           //Offset originale del collider del player.

    [SerializeField] LayerMask layerTerreno;                    //Layer del terreno.
    LevelManager levelManager;                                  //Level Manager.
    GameObject scala;                                           //Scala.
    Vector2 posBombHurtsPlayer;                                 //Posizione della bomba che colpisce il player.

    // Start is called before the first frame update
    void Start()
    {
        playerRB = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerCollider = GetComponent<CapsuleCollider2D>();
        comandiPlayer = /*/new ComandiComputer()/**//**/new ComandiSmartphone()/**/;
        jumping = false;
        hurting = false;
        posizioneIniziale = transform.position;
        jumpSound.volume = GManager.gameManager.livelloSuoni;
        hurtedSound.volume = GManager.gameManager.livelloSuoni;
        oggettoPickupSound.volume = GManager.gameManager.livelloSuoni;

        sizeColliderOriginale = playerCollider.size;
        offesetColliderOriginale = playerCollider.offset;
        
        levelManager = GameObject.FindGameObjectWithTag("level_manager").gameObject.GetComponent<LevelManager>();

        //Elimina il joystick dallo schermo se non viene usato i comandi dello smartphone.
        if (!(comandiPlayer is ComandiSmartphone))
            Destroy(GameObject.FindGameObjectWithTag("joystick"));

        //Viene ripristinato la vita che aveva nel livello precendente finito.
        if (GManager.gameManager.vitaPlayer != 0)
            vita = GManager.gameManager.vitaPlayer;
    }

    void FixedUpdate()
    {
        if(vita<=0)
        {
            levelManager.SetPartitaPersa(true);
        }

        if (!hurting)
            Movement();

        PlayerAnimations();
        Hurt();
        SincronizzazioneColpoEColpoBomba();
        CheckGrounded();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Termina lo stato di hurt di player quando tocca il terreno
        if(collision.collider.tag.Equals("terreno") && hurting)
        {
            playerAnimator.SetBool("IsHurt", false);
            hurting = false;
            tempoAnimHurt = 0.0f;
        }

        //Termina lo stato di salto del player quando tocca il terreno.
        if (collision.collider.tag.Equals("terreno") && jumping)
        {
            jumping = false;
        }

        //Il player scendendo le scale tocca il terreno?
        if(collision.collider.tag.Equals("terreno") && muoviSuScale)
        {
            SetMovimentiSuTerreno();
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("key") || collision.gameObject.tag.Equals("vita") || collision.gameObject.tag.Equals("diamond"))
        {
            oggettoPickupSound.Play();
        }

        //Il player "collide" la chiave?
        if(collision.gameObject.tag.Equals("key"))
        {
            Destroy(collision.gameObject);
            haveKey = true;
        }

        //Il player tocca le scale?
        if(collision.gameObject.tag.Equals("scala") && !hurting)
        {
            muoviSuScale = true;
            scala = collision.gameObject;
        }

        //Il player tocca le scale quando non e' sul terreno?
        if (collision.gameObject.tag.Equals("scala") && !isGrounded && !hurting)
        {
            muoviSuScale = true;
            scala = collision.gameObject;
            SetMovimentiSuScale(scala.transform.position);

            jumping = false;
        }

        //Il player tocca le scale quando e' in cima?
        if (collision.gameObject.tag.Equals("scala") && isSuScalaCima && !hurting)
        {
            muoviSuScale = true;
            scala = collision.gameObject;
            SetMovimentiSuScale(scala.transform.position);
        }

        //IL player e' alla cima dele scale?
        if (collision.gameObject.tag.Equals("scala_cima"))
        {
            isSuScalaCima = true;
        }

        //Il player e' arrivato alla cima delle scale muovendosi su di esse?
        if(collision.gameObject.tag.Equals("scala_cima") && muoviSuScale)
        {
            SetMovimentiSuTerreno();
            Jump();
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        //Esce dalla cima delle scale
        if (collision.gameObject.tag.Equals("scala_cima"))
        {
            isSuScalaCima = false;
        }
    }

    /// <summary>
    /// Indica o meno se il player salta.
    /// </summary>
    public bool jumping { set; get; }

    /// <summary>
    /// Indica o meno se il player e' stato colpito.
    /// </summary>
    public bool hurting { set; get; }

    /// <summary>
    /// Numero di vite massime del player.
    /// </summary>
    /// <returns>Numero di vite massime del player.</returns>
    public int VitaMax()
    {
        return vitaMax;
    }

    /// <summary>
    /// Restituise la velocita' di salto del player.
    /// </summary>
    /// <returns>Velocita' di salto del player.</returns>
    public float SpeedJump()
    {
        return speedJump;
    }

    /// <summary>
    /// Indica se il player ha preso la chiave o meno.
    /// </summary>
    /// <returns></returns>
    public bool HaveKey()
    {
        return haveKey;
    }

    /// <summary>
    /// Verifica se il player si trova sul terreno.
    /// </summary>
    /// <returns>true se il player si trova sul terreno, false altrimenti.</returns>
    public bool IsGrounded()
    {
        return isGrounded;
    }

    /// <summary>
    /// Incrementa di una vita al player.
    /// </summary>
    public void IncrementaVita()
    {
        if (vita < vitaMax)
            vita++;
    }

    /// <summary>
    /// Mette il player nella posizione in cui e' spawnato all'inizio del livello.
    /// </summary>
    public void MettiNellaPosizioneIniziale()
    {
        transform.position = new Vector3(posizioneIniziale.x,
                                         posizioneIniziale.y,
                                         posizioneIniziale.z);
    }

    /// <summary>
    /// Quando un oggetto colpisce il player, li fa perdere una vita e lo spinge in un'altra direzione.
    /// </summary>
    /// <param name="velocitaHurt">Velocita' di spostamento e direzione per spingere il player</param>
    public void PlayerHurted(Vector2 velocitaHurt)
    {
        SetMovimentiSuTerreno();

        hurtedSound.Play();
        jumping = true;
        hurting = true;
        playerRB.velocity = new Vector2(velocitaHurt.x, velocitaHurt.y);
        vita--;

        if (velocitaHurt.x < 0)
        {
            playerRB.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            playerRB.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    /// <summary>
    /// Imposta che il player deve essere colpito dalla bomba (che "richiama" questo metodo).
    /// </summary>
    /// <param name="posizioneBomba">Posizione della bomba che colpira' il player.</param>
    public void HurtPlayerByBomb(Vector3 posizioneBomba)
    {
        sincronizzaColpoDaBomba = true;
        posBombHurtsPlayer = new Vector2(posizioneBomba.x, posizioneBomba.y);
    }

    /// <summary>
    /// Imposta i movimenti normali del player di quando non e' sulle scale.
    /// </summary>
    void SetMovimentiSuTerreno()
    {
        playerRB.constraints = RigidbodyConstraints2D.FreezeRotation;
        muoviSuScale = false;
        playerRB.gravityScale = 1f;

        playerCollider.size = sizeColliderOriginale;
        playerCollider.offset = offesetColliderOriginale;
    }

    /// <summary>
    /// Imposta i movimenti del player sulle scale.
    /// </summary>
    void SetMovimentiSuScale(Vector3 posizioneScala)
    {
        playerCollider.size = new Vector2(0.154f, 0.201f);
        playerCollider.offset = new Vector2(0.0f, -0.040f);

        playerRB.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        transform.position = new Vector3(posizioneScala.x, transform.position.y, transform.position.z);
        playerRB.gravityScale = 0f;
    }

    /// <summary>
    /// Il player salta.
    /// </summary>
    public void Jump()
    {
        playerRB.velocity = new Vector2(playerRB.velocity.x, speedJump);
        jumping = true;
        isGrounded = false;
    }

    //Si fa muovere il player.
    void Movement()
    {
        //Il player si muove sulle scale.
        if (muoviSuScale)
            Climb();

        //Imposta che il player si muove sulle scale.
        if(comandiPlayer.SiMuoveSulleScale() && muoviSuScale)
        {
            if (!comandiPlayer.SaliScalaVersoGiu() || !isGrounded)
                SetMovimentiSuScale(scala.transform.position);

            return;
        }

        //Sposta player verso sinistra.
        if (comandiPlayer.SpostaPlayerASinistra())
        {
            playerRB.velocity = new Vector2(-speedMove, playerRB.velocity.y);
            playerRB.transform.localScale = new Vector3(-1, 1, 1);
        }

        //Sposta player verso destra.
        if (comandiPlayer.SpostaPlayerADestra())
        {
            playerRB.velocity = new Vector2(speedMove, playerRB.velocity.y);
            playerRB.transform.localScale = new Vector3(1, 1, 1);
        }

        //Fa saltare player.
        if (comandiPlayer.PlayerSalta() && !jumping)
        {
            jumpSound.Play();
            Jump();
        }
    }

    //Fa muovere il player sulle scale.
    void Climb()
    {
        //Il player salta dalla scala.
        if(comandiPlayer.PlayerSalta())
        {
            SetMovimentiSuTerreno();
            Jump();

            return;
        }

        //Il player sale verso su le scale.
        if(comandiPlayer.SaliScalaVersoSu() && !isSuScalaCima)
        {
            playerRB.velocity = new Vector2(0f, speedMove);
            playerAnimator.speed = 1f;
        }
        //Il player sale verso in giu' le scale.
        else if(comandiPlayer.SaliScalaVersoGiu() && !isGrounded)
        {
            playerRB.velocity = new Vector2(0f, -speedMove);
            playerAnimator.speed = 1f;
        }
        //Resta fermo sulle scale.
        else if(!isGrounded)
        {
            playerRB.velocity = new Vector2(0f, 0f);
            playerAnimator.speed = 0f;
        }
    }

    //Imposta le animazioni del player durante il gioco.
    void PlayerAnimations()
    {
        //Il player non e' sulle scale?
        if (!muoviSuScale)
        {
            playerAnimator.SetBool("IsJumping", (playerRB.velocity.y > 0) && !isGrounded);
            playerAnimator.SetBool("IsFalling", (playerRB.velocity.y < 0) && !isGrounded);
            playerAnimator.SetBool("isMoving", comandiPlayer.IsMoving() && isGrounded);
            playerAnimator.SetBool("IsClimb", false);
            playerAnimator.speed = 1f;
        }
        else
        {
            playerAnimator.SetBool("IsJumping", false);
            playerAnimator.SetBool("IsFalling", false);
            playerAnimator.SetBool("isMoving", comandiPlayer.IsMoving() && isGrounded);
            playerAnimator.SetBool("IsClimb", !isGrounded);
        }
    }

    void Hurt()
    {
        if(hurting)
        {
            if(!playerAnimator.GetBool("IsHurt"))
            {
                playerAnimator.SetBool("IsHurt", true);
            }

            tempoAnimHurt += Time.fixedDeltaTime;

            //Termina lo stato di hurt
            if(tempoAnimHurt >= DURATA_STATO_HURT)
            {
                playerAnimator.SetBool("IsHurt", false);
                hurting = false;
                tempoAnimHurt = 0.0f;
            }
        }
        else
        {
            playerAnimator.SetBool("IsHurt", false);
            hurting = false;
            tempoAnimHurt = 0.0f;
        }
    }

    void SincronizzazioneColpoEColpoBomba()
    {
        if (!sincronizzaColpoDaBomba) return;
        
        tempoAttesaPerColpoBomba += Time.fixedDeltaTime;
        float durataAnimExplosion = GameObject.FindGameObjectWithTag("explosion").gameObject.GetComponent<Explosion>().DurataAnimazioneEsplosione();
        
        if (tempoAttesaPerColpoBomba >= 0.5f / 11 * durataAnimExplosion)
        {
            if(transform.position.x < posBombHurtsPlayer.x)
            {
                PlayerHurted(new Vector2(-VELOCITA_X_PLAYER_HURTED, VELOCITA_Y_PLAYER_HURTED));
            }
            else
            {
                PlayerHurted(new Vector2(VELOCITA_X_PLAYER_HURTED, VELOCITA_Y_PLAYER_HURTED));
            }

            sincronizzaColpoDaBomba = false;
            tempoAttesaPerColpoBomba = 0f;
        }
    }

    //Verifica se il player collide sul terreno.
    void CheckGrounded()
    {
        //Il player e' poggiato sul terreno?
        if(Physics2D.Raycast(transform.position, Vector2.down, 0.2f, layerTerreno))
        {
            isGrounded = true;
        }
        //Il player non e' poggiato sul terreno.
        else
        {
            isGrounded = false;

            //Il player tocca la parte laterale (sinistra o destra) del terreno?
            const float RAGGIO_AZIONE = 0.12f;

            if (Physics2D.Raycast(transform.position, Vector2.left, RAGGIO_AZIONE, layerTerreno) ||
                Physics2D.Raycast(transform.position, Vector2.right, RAGGIO_AZIONE, layerTerreno))
            {
                playerRB.velocity = new Vector2(0.0f, playerRB.velocity.y);
            }
        }
    }
}
