﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComandiComputer : Comandi
{
    public override bool SpostaPlayerADestra()
    {
        return Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
    }

    public override bool SpostaPlayerASinistra()
    {
        return Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);
    }

    public override bool SaliScalaVersoSu()
    {
        return Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow);
    }

    public override bool SaliScalaVersoGiu()
    {
        return Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow);
    }

    public override bool PlayerSalta()
    {
        return Input.GetKey(KeyCode.Space);
    }
}
