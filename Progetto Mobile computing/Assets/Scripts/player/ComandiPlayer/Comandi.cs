﻿using System.Collections;
using System.Collections.Generic;

public abstract class Comandi 
{
    /// <summary>
    /// Si vuole spostare il player verso destra.
    /// </summary>
    /// <returns>true se si vuole spostare il player verso destra, false altrimenti.</returns>
    abstract public bool SpostaPlayerADestra();

    /// <summary>
    /// Si vuole spostare il player verso sinistra.
    /// </summary>
    /// <returns>true se si vuole spostare il player verso sinistra, false altrimenti.</returns>
    abstract public bool SpostaPlayerASinistra();

    /// <summary>
    /// Si vuole far salire il player le scale verso su.
    /// </summary>
    /// <returns>true se si vuole far salire il player le scale verso su, false altrimenti.</returns>
    abstract public bool SaliScalaVersoSu();

    /// <summary>
    /// Si vuole far salire il player le scale verso giu'.
    /// </summary>
    /// <returns>true se si vuole far salire il player le scale verso giu', false altrimenti.</returns>
    abstract public bool SaliScalaVersoGiu();

    /// <summary>
    /// Si vuole far saltare il player.
    /// </summary>
    /// <returns>true se si vuole far saltare il player, false altrimenti.</returns>
    abstract public bool PlayerSalta();

    /// <summary>
    /// Indica se il player si sta spostando verso sinistra o destra.
    /// </summary>
    /// <returns>true se il player si sta spostando verso sinistra o destra, false altrimenti.</returns>
    public bool IsMoving()
    {
        return SpostaPlayerADestra() ||
               SpostaPlayerASinistra();
    }

    /// <summary>
    /// Indica se il player si sta muovendo sulle scale.
    /// </summary>
    /// <returns></returns>
    public bool SiMuoveSulleScale()
    {
        return SaliScalaVersoSu() || SaliScalaVersoGiu();
    }
}
