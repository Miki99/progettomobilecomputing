﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComandiSmartphone : Comandi
{
    private FixedJoystick joystick;

    public ComandiSmartphone()
    {
        this.joystick = GameObject.FindGameObjectWithTag("joystick").GetComponent<FixedJoystick>();
    }

    public override bool PlayerSalta()
    {
        //Non viene toccato il touchscreen.
        if (Input.touchCount <= 0) return false;

        //Viene toccato il touchscreen e verifica se e' stato toccato nella nella posizione giusta.
        float altezzaSchermo = Camera.main.scaledPixelHeight;
        float larghezzaSchermo = Camera.main.scaledPixelWidth;
        Vector2 posizione_touch;
        
        foreach(Touch t in Input.touches)
        {
            posizione_touch = t.position;

            if (posizione_touch.x >= larghezzaSchermo / 2 && posizione_touch.y <= 2.0f / 3 * altezzaSchermo)
                return true;
        }

        return false;
    }

    public override bool SpostaPlayerADestra()
    {
        return (joystick.Horizontal > 0.3f);
    }

    public override bool SpostaPlayerASinistra()
    {
        return (joystick.Horizontal < -0.3f);
    }

    public override bool SaliScalaVersoSu()
    {
        return (joystick.Vertical > 0.3f);
    }

    public override bool SaliScalaVersoGiu()
    {
        return (joystick.Vertical < -0.3f);
    }
}
