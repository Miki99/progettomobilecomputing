﻿using Firebase.Auth;
using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GManager : MonoBehaviour
{
    public static GManager gameManager;

    //Dati del game manager.
    public FirebaseUser utente;                         //Profilo dell'utente
    public Task<DataSnapshot> dbReadClassificaTask;

    //Dati della partita.
    public int vitaPlayer = 0;
    public int punteggioTotaleAttuale = 0;

    //Valori di volume dei suoni del gioco.
    public float livelloMusica;
    public float livelloSuoni;

    void Awake()
    {
        if(gameManager == null)
        {
            DontDestroyOnLoad(gameObject);
            gameManager = this;

            CaricaPreferenze();
            GetComponent<AudioSource>().volume = livelloMusica;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        FirebaseAuth auth = FirebaseAuth.DefaultInstance;
        auth.SignOut();

        SalvaPreferenze();
    }

    void CaricaPreferenze()
    {
        livelloMusica = PlayerPrefs.GetFloat("livello_musica", 0.67f);
        livelloSuoni = PlayerPrefs.GetFloat("livello_suoni", 1.0f);
    }

    public void SalvaPreferenze()
    {
        PlayerPrefs.SetFloat("livello_musica", livelloMusica);
        PlayerPrefs.SetFloat("livello_suoni", livelloSuoni);
    }
}
