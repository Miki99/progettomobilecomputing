﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatiPartita : MonoBehaviour
{
    //Dati del player.
    public int vita;                                        //Numero di vite del player.

    //Dati della partita.
    public int punteggioTotaleAttuale;                      //Punteggio totalizzato attualmente dal player.

    public DatiPartita()
    {
        vita = GameObject.FindGameObjectWithTag("player").gameObject.GetComponent<Player>().vita;
        punteggioTotaleAttuale = 0;
    }
}
