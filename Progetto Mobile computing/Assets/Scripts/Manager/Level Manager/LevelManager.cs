﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [SerializeField] string nomeLivello;                        //Nome del livello attuale
    [SerializeField] string nomeLivelloSuccesso;                //Nome del livello successivo.
    [SerializeField] int timerLivello = 0;                      //Durata di tempo (in secondi) per completare il livello attuale.
    [SerializeField] bool ultimoLivello = false;                //Indica se il livello e' l'ultimo del gioco o no.
    int punteggio_totale_iniziale;                              //Punteggio totalizzato dal player nei livelli precedenti.
    int punteggio_livello = 0;                                  //Punteggio totalizzato del livello attuale.
    float cronometro = 0f;                                      //Cronometra il tempo (in secondi) che impiega il player per completare il livello attuale.
    bool isCompleted = false;                                   //Indica se e' stato completato o meno il livello attuale.
    bool isPersa = false;                                       //Indica se la partita e' stata persa o meno.
    bool isVinta = false;                                       //Indica se la partita e' stata vinta o meno.
    bool pannelloPartitaAttivato = false;                       //Indica se ha attivato o meno il pnl_partitaPersaOVinta.

    public GameObject gestoreScenaLivello;                      //Gestore della scena di livello.

    // Start is called before the first frame update
    void Start()
    {
        punteggio_totale_iniziale = GManager.gameManager.punteggioTotaleAttuale;
    }

    // Update is called once per frame
    void Update()
    {
        //Il tempo e' scaduto?
        if (TempoRimasto() <= 0f)
        {
            isPersa = true;
        }

        if ((isPersa || isVinta) && !pannelloPartitaAttivato)
        {
            pannelloPartitaAttivato = true;
            PartitaConclusa();
        }

        if (!isCompleted && !isPersa)
            cronometraTempo();

        if (isCompleted && !isPartitaVinta())
        {
            CambiaLivello();
            print("Livello completato");
        }
    }

    /// <summary>
    /// Cronometra il temmpo del player per finire il livello.
    /// </summary>
    void cronometraTempo()
    {
        cronometro += Time.deltaTime;
    }

    /// <summary>
    /// Restituisce il punteggio totalizzato attualmente nei vari livelli. 
    /// </summary>
    /// <returns>Punteggio totalizzato attualmente nei vari livelli.</returns>
    public int GetPunteggioTotaleAttuale()
    {
        return punteggio_totale_iniziale + punteggio_livello;
    }

    /// <summary>
    /// Restituisce il tempo rimanente per terminare il livello.
    /// </summary>
    /// <returns>Tempo rimanente per terminare il livello.</returns>
    public float TempoRimasto()
    {
        return ((float)timerLivello) - cronometro;
    }

    /// <summary>
    /// Indica se il player ha vinto la partita o meno.
    /// </summary>
    /// <returns>true se il player jha vinto la partita, false altrimenti.</returns>
    public bool isPartitaVinta()
    {
        return isVinta && !isPersa;
    }

    /// <summary>
    /// Imposta che la partita e' persa o meno.
    /// </summary>
    /// <param name="persa">Valore per indicare se la partita e' persa o meno.</param>
    public void SetPartitaPersa(bool persa)
    {
        isPersa = persa;
    }

    /// <summary>
    /// Imposta che il livello e' stato completato o meno.
    /// </summary>
    /// <param name="completato">Valore che indica se e' stato completato o meno il livello.</param>
    public void SetLivelloCompletato(bool completato)
    {    
        isCompleted = completato;

        if (ultimoLivello)
        {
            isVinta = true;
            isPersa = false;
        }
    }

    void CambiaLivello()
    {
        //Vengono salvati temporaneamente la vita del player e il punteggio totalizzato.
        GManager.gameManager.vitaPlayer = GameObject.FindGameObjectWithTag("player").GetComponent<Player>().vita;
        GManager.gameManager.punteggioTotaleAttuale = GetPunteggioTotaleAttuale();

        if(!isVinta)
        {
            //Si passa al livello successivo.
            SceneManager.LoadScene(nomeLivelloSuccesso);
        }
    }

    void PartitaConclusa()
    {
        //print("Partita persa.");
        gestoreScenaLivello.GetComponent<GestoreScenaLivello>().attivaPannelloDiPartitaPersaOVinta();
    }

    /// <summary>
    /// Analizza l'oggetto e aggiorna le statistiche (punteggio, etc.) se l'oggetto e' un nemico o un diamante.
    /// </summary>
    /// <param name="oggetto">Oggetto da analizzare e che aggiorna le statistiche.</param>
    public void analizzaOggettoEAggiornaStatistiche(GameObject oggetto)
    {
        if (oggetto.tag.Equals("diamond"))
        {
            punteggio_livello += oggetto.GetComponent<Diamond>().GetPunteggio();
        }
        else if (oggetto.tag.Equals("frog"))
        {
            punteggio_livello += oggetto.GetComponent<Frog>().GetPunteggio();
        }
        else if (oggetto.tag.Equals("eagle"))
        {
            punteggio_livello += oggetto.GetComponent<Eagle>().GetPunteggio();
        }
        else if (oggetto.tag.Equals("slug"))
        {
            punteggio_livello += oggetto.GetComponent<Slug>().GetPunteggio();
        }
        else if (oggetto.tag.Equals("piranha_plant"))
        {
            punteggio_livello += oggetto.GetComponent<PiranhaPlant>().GetPunteggio();
        }
    }
}
