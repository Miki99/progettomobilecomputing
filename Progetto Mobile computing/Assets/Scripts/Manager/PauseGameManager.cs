﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGameManager : MonoBehaviour
{
    bool isPausa = false;
    public GameObject pnlPause;

    /// <summary>
    /// Cambia lo stato di pausa di gioco
    /// </summary>
    public void CambiaStatoDiPausaDiGioco()
    {
        isPausa = !isPausa;

        if (isPausa)
        {
            //Mette in pausa il gioco.
            Time.timeScale = 0f;
        }
        else
        {
            //Riprende il gioco
            Time.timeScale = 1f;
        }
        pnlPause.SetActive(isPausa);
    }

    /// <summary>
    /// Ricomincia la partita.
    /// </summary>
    public void RicominciaPartita()
    {
        Time.timeScale = 1f;
        GManager.gameManager.vitaPlayer = 0;
        GManager.gameManager.punteggioTotaleAttuale = 0;
        SceneManager.LoadScene("Livello1");
    }

    /// <summary>
    /// Esce dal livello del gioco e torna nella schermata principale.
    /// </summary>
    public void EsciDalLivello()
    {
        Time.timeScale = 1f;

        GManager.gameManager.vitaPlayer = 0;
        GManager.gameManager.punteggioTotaleAttuale = 0;
        SceneManager.LoadScene("MenuPrincipale");
    }
}
