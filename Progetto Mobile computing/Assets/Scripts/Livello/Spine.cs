﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spine : MonoBehaviour
{
    const float VELOCITA_X_PLAYER_HURTED = 1.5f;            //Velocita' x del player colpito.
    const float VELOCITA_Y_PLAYER_HURTED = 2.0f;            //Velocita' y del player colpito.
    const float VELOCITA_Y_TOP_PLAYER_HURTED = 1.0f;        //Velocita' y del player colpito dalle spine in alto.

    [SerializeField] bool isTop = false;                    //Indica se le spine sono in cima o meno.

    void OnTriggerEnter2D(Collider2D collision)
    {
        //Il player ha colliso sulle "spine"?
        if(collision.gameObject.tag.Equals("player"))
        {
            GameObject player = collision.gameObject;
            Rigidbody2D playerRB = player.GetComponent<Rigidbody2D>();

            if(player.transform.position.x < transform.position.x)
            {
                if(isTop) 
                    player.GetComponent<Player>().PlayerHurted(new Vector2(playerRB.velocity.x, -VELOCITA_Y_TOP_PLAYER_HURTED));
                else
                    player.GetComponent<Player>().PlayerHurted(new Vector2(-VELOCITA_X_PLAYER_HURTED, VELOCITA_Y_PLAYER_HURTED));
            }
            else
            {
                if(isTop)
                    player.GetComponent<Player>().PlayerHurted(new Vector2(playerRB.velocity.x, -VELOCITA_Y_TOP_PLAYER_HURTED));
                else
                    player.GetComponent<Player>().PlayerHurted(new Vector2(VELOCITA_X_PLAYER_HURTED, VELOCITA_Y_PLAYER_HURTED));
            }
        }
    }
}
