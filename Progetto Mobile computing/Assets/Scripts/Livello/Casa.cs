﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//La casa e' l'obiettivo da raggiungere, ovvero trovare la chiave dentro in una delle tante chest 
//situate nella mappa e trovare la casa per aprire la porta. 
public class Casa : MonoBehaviour
{
    public const float DISTANZA_X_APRI_PORTA = 0.65f;           //Distanza x massima che il player puo' aprire la porta.
    public const float DISTANZA_Y_APRI_PORTA = 0.55f;           //Distanza y massima che il player puo' aprire la porta.

    Animator anim;                                              //Animator della casa.

    Player player;                                              //Istanza del player.
    LevelManager levelManager;                                  //Istanza del level manager.

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("player").gameObject.GetComponent<Player>();
        levelManager = GameObject.FindGameObjectWithTag("level_manager").gameObject.GetComponent<LevelManager>();
    }

    private void Update()
    {
        //Il player apre la porta della casa con la chiave?
        if(Mathf.Abs(player.transform.position.x - transform.position.x)<=DISTANZA_X_APRI_PORTA &&
           Mathf.Abs(player.transform.position.y - transform.position.y)<= DISTANZA_Y_APRI_PORTA &&
           player.HaveKey() && !anim.GetBool("Open"))
        {
            anim.SetBool("Open", true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Il player entra nella casa?
        if(collision.gameObject.tag.Equals("player") && player.HaveKey())
        {
            levelManager.SetLivelloCompletato(true);
        }
        
    }
}
