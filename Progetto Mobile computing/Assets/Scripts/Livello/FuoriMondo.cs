﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuoriMondo : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("player"))
        {
            Player p = collision.gameObject.GetComponent<Player>();

            p.PlayerHurted(new Vector2(0f, 0f));
            p.MettiNellaPosizioneIniziale();
        }
        else
        {
            Destroy(collision.gameObject);
        }
    }
}
