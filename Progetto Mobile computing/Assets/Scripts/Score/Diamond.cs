﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour
{
    public const int PUNTEGGIO_DIAMOND = 10;                    //Punteggio del diamond.

    GameObject levelManager;                                    //Game Object del Level Manager.

    private void Start()
    {
        levelManager = GameObject.FindGameObjectWithTag("level_manager");
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //Il player ha "colpito" il diamond?
        if(collision.gameObject.tag.Equals("player"))
        {
            levelManager.GetComponent<LevelManager>().analizzaOggettoEAggiornaStatistiche(gameObject);

            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Restituisce il punteggio del diamante quando viene raccolto dal player.
    /// </summary>
    /// <returns>Punteggio del diamante raccolto.</returns>
    public int GetPunteggio()
    {
        return PUNTEGGIO_DIAMOND;
    }
}
