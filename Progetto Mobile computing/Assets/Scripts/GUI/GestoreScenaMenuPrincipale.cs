﻿using Firebase;
using Firebase.Auth;
using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GestoreScenaMenuPrincipale : MonoBehaviour
{
    public GameObject pnl_impostazioni;                 //Pannello delle impostazioni.
    public Slider sliderbarMusica;                      //Sliderbar di musica.
    public Slider sliderbarSuoni;                       //Sliderbar di suoni.
    
    private FirebaseUser utente;
    DatabaseReference dbReference;

    void Awake()
    {
        //Check that all of the necessary dependencies for Firebase are present on the system
        var initTask = FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                dbReference = FirebaseDatabase.DefaultInstance.RootReference;
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void Start()
    {
        sliderbarMusica.value = GManager.gameManager.livelloMusica;
        sliderbarSuoni.value = GManager.gameManager.livelloSuoni;

        utente = GManager.gameManager.utente;
        Debug.LogFormat("- GestoreScenaMenuPrincipale: Utente = {0}, ({1})", utente.DisplayName, utente.Email);
    }

    private void Update()
    {
        GManager.gameManager.GetComponent<AudioSource>().volume = sliderbarMusica.value;
    }

    public void GiocaPartita()
    {
        SceneManager.LoadScene("Livello1");
    }

    public void PulsanteClassifica()
    {
        StartCoroutine(VaiInScenaClassifica());
    }

    private IEnumerator VaiInScenaClassifica()
    {
        var dbReadTask = dbReference.Child("utenti").OrderByChild("punteggio").GetValueAsync();

        yield return new WaitUntil(predicate: () => dbReadTask.IsCompleted);

        GManager.gameManager.dbReadClassificaTask = dbReadTask;
        SceneManager.LoadScene("Classifica");
    }

    public void PulsanteDisconnetti()
    {
        FirebaseAuth auth = FirebaseAuth.DefaultInstance;

        print("- GestoreScenaMenuPrincipale: Profilo disconnesso.");
        print("- GestoreScenaMenuPrincipale: utente != null ? " + (utente!=null));

        auth.SignOut();
        SceneManager.LoadScene("SchermataIniziale");
    }

    public void PulsanteOpzioni()
    {
        pnl_impostazioni.SetActive(true);
    }

    public void PulsanteChiudi()
    {
        GManager.gameManager.livelloMusica = sliderbarMusica.value;
        GManager.gameManager.livelloSuoni = sliderbarSuoni.value;
        GManager.gameManager.SalvaPreferenze();

        pnl_impostazioni.SetActive(false);
    }
}
