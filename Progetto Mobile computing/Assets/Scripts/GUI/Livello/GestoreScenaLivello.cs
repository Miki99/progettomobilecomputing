﻿using Firebase;
using Firebase.Auth;
using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GestoreScenaLivello : MonoBehaviour
{
    const float TEMPO_ATTESA_APERTURA_PANNELLO_PARTITA_PERSA = 0.125f;          //Tempo di attesa prima che si apre il pannello della partita persa.

    public GameObject pnl_partitaPersaOVinta;                                   //Pannello della partita persa o vinta.
    public GameObject pnl_partitaTerminata;                                     //Pannello della partita terminata.
    public Text txt_partitaPersaOVinta;                                         //Testo del punteggio sul popup del pannello di partita persa o vinta.
    public Text txt_msgPunteggioDiPartitaPersa;                                 //Testo del punteggio sul popup del pannello di partita persa o vinta.
    float tempoAttesa = 0.0f;
    bool attendiAperturaPannelloPartitaPersaOVinta = false;

    LevelManager levelManager;                                                  //Level manager.
    DatabaseReference dbReference;

    void Awake()
    {
        //Check that all of the necessary dependencies for Firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                dbReference = FirebaseDatabase.DefaultInstance.RootReference;
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void Start()
    {
        levelManager = GameObject.FindGameObjectWithTag("level_manager").GetComponent<LevelManager>();
    }

    private void Update()
    {
        if(attendiAperturaPannelloPartitaPersaOVinta)
        {
            tempoAttesa += Time.deltaTime;

            if(tempoAttesa >= TEMPO_ATTESA_APERTURA_PANNELLO_PARTITA_PERSA)
            {
                attendiAperturaPannelloPartitaPersaOVinta = false;
                apriPannelloPartitaPersaOVinta();
            }
        }
    }

    //Il pulsante Ricomincia fa rigiocare al player la partita.
    public void PulsanteRigioca()
    {
        //"Riprende" il tempo
        Time.timeScale = 1f;

        GManager.gameManager.vitaPlayer = 0;
        GManager.gameManager.punteggioTotaleAttuale = 0;
        SceneManager.LoadScene("Livello1");
    }

    public void PulsanteEsciDiPannelloPartitaTerminata()
    {
        //"Riprende" il tempo
        Time.timeScale = 1f;

        GManager.gameManager.vitaPlayer = 0;
        GManager.gameManager.punteggioTotaleAttuale = 0;
        SceneManager.LoadScene("MenuPrincipale");
    }

    //Il pulsante Chiudi di pnl_partitaPersaOVinta salva il punteggio totalizzato (se e' il migliore) e passa nella scena prinicipale.
    public void pulsanteChiudi()
    {
        StartCoroutine(SalvaPunteggio(levelManager.GetPunteggioTotaleAttuale()));
    }

    private void VaiAlPannelloPartitaTerminata()
    {
        pnl_partitaPersaOVinta.SetActive(false);
        pnl_partitaTerminata.SetActive(true);
    }

    //Salva il punteggio totalizzato dal player e la partita persa.
    private IEnumerator SalvaPunteggio(int punteggioTotalizzato)
    {
        FirebaseUser utente = GManager.gameManager.utente;

        //Legge il punteggio totalizzato in una partita precendete del player.
        var dbReadTask = dbReference.Child("utenti").Child(utente.UserId).GetValueAsync();

        yield return new WaitUntil(predicate: () => dbReadTask.IsCompleted);

        //E' fallito la lettura al database?
        if(dbReadTask.Exception != null)
        {
            Debug.LogError($"Lettura al database fallita. Eccezzione = {dbReadTask.Exception}");
        }
        else
        {
            Debug.Log("Lettura al database completata.");

            //L'utente aveva gia' il punteggio salvato di una partita gia' giocata?
            if(dbReadTask.Result.Value != null)
            {
                DataSnapshot datiUtente = dbReadTask.Result;
                int punteggioDB = int.Parse(datiUtente.Child("punteggio").Value.ToString());
                //print("punteggioDB = " + punteggioDB);

                //Non viene salvato punteggioTotalizzato nel database perche' non e' il suo miglior punteggio fatto.
                if (punteggioDB > punteggioTotalizzato)
                {
                    Debug.Log($"Punteggio totalizzato non salvato.");
                    VaiAlPannelloPartitaTerminata();

                    yield break;
                } 
            }

            //Viene salvato su database il punteggio fatto e la partita completata o meno.
            //Salva sul database l'username.
            var dbSaveTask = dbReference.Child("utenti").Child(utente.UserId).Child("username").SetValueAsync(utente.DisplayName);

            yield return new WaitUntil(predicate: () => dbSaveTask.IsCompleted);
            if (dbSaveTask.Exception != null)
            {
                Debug.LogError($"Fallito il salvataggio dei dati utente nel database. Eccezione = {dbSaveTask.Exception}");
                VaiAlPannelloPartitaTerminata();
                yield break;
            }

            //Salva il punteggio totalizzato del player.
            dbSaveTask = dbReference.Child("utenti").Child(utente.UserId).Child("punteggio").SetValueAsync(punteggioTotalizzato);

            yield return new WaitUntil(predicate: () => dbSaveTask.IsCompleted);

            if(dbSaveTask.Exception != null)
            {
                Debug.LogError($"Fallito il salvataggio dei dati utente nel database. Eccezione = {dbSaveTask.Exception}");
                VaiAlPannelloPartitaTerminata();
            }
            else
            {
                print("Completato il salvataggio dei dati utente nel database.");
            }
        }

        VaiAlPannelloPartitaTerminata();
    }

    public void attivaPannelloDiPartitaPersaOVinta()
    {
        attendiAperturaPannelloPartitaPersaOVinta = true;
    }

    private void apriPannelloPartitaPersaOVinta()
    {
        pnl_partitaPersaOVinta.SetActive(true);
        txt_msgPunteggioDiPartitaPersa.text = "Il puteggio totalizzato e' " + levelManager.GetPunteggioTotaleAttuale();
        if(levelManager.isPartitaVinta())
        {
            txt_partitaPersaOVinta.text = "Hai vinto!";
            txt_partitaPersaOVinta.color = Color.green;
        }
        else
        {
            txt_partitaPersaOVinta.text = "Hai perso!";
        }

        //Mette in "pausa" il tempo.
        Time.timeScale = 0f;
    }
}
