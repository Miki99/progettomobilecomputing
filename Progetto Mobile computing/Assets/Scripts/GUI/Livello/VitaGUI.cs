﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VitaGUI : MonoBehaviour
{
    private const float DISTANZA_VITE = 90.0f;                  //Distanziamento tra vitaImg e un'altra in pixel.
    private const float WIDTH_SCHERMO_DEFAULT = 1280.0f;        //Larghezza dello schermo di default;
    private GameObject player;                                  //Player.
    public Image vitaImg;                                       //Image della vita del player.
    private Image[] viteImg;                                    //Array di Image della vita del player.

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("player").gameObject;
        //vitaImg = GetComponent<Image>();
        viteImg = new Image[player.GetComponent<Player>().VitaMax()];

        viteImg[0] = vitaImg;
        for (int i = 1; i < player.GetComponent<Player>().vita; i++)
        {
            aggiungiUnaVita();
        }
    }

    // Update is called once per frame
    void Update()
    {
        int n_vitaPlayer = player.GetComponent<Player>().vita;
        int n_vitaImg = numeroImgsVita();

        if(n_vitaPlayer >= 0)
        {
            if (n_vitaPlayer < n_vitaImg)
                eliminaUnaVita();
            else if (n_vitaPlayer > n_vitaImg)
                aggiungiUnaVita();
        }
    }

    //Aggiunge un'Image di vita in viteImg.
    void aggiungiUnaVita()
    {
        bool aggiunto = false;
        float scale = Camera.main.scaledPixelWidth/WIDTH_SCHERMO_DEFAULT;

        for (int i = 0; (i < player.GetComponent<Player>().VitaMax()) && !aggiunto; i++)
        {
            if (viteImg[i] == null)
            {
                viteImg[i] = (Image)Instantiate(vitaImg,
                                                vitaImg.transform.position + i * (new Vector3(DISTANZA_VITE * scale, 0f, 0f)),
                                                vitaImg.transform.rotation,
                                                transform);
                aggiunto = true;
            }
        }
    }

    //Toglie un'Image di vita in viteImg.
    void eliminaUnaVita()
    {
        bool eliminato = false;

        for (int i = viteImg.Length-1; (i >= 0) && !eliminato; i--)
        {
            if (viteImg[i] != null)
            {
                Image img = viteImg[i];
                Destroy(img);

                viteImg[i] = null;
                eliminato = true;
            }
        }
    }

    //Conta quante Image di vita ci sono nell'array viteImg.
    private int numeroImgsVita()
    {
        int n_img = 0;

        for(int i = 0; i < viteImg.Length; i++)
        {
            if (viteImg[i] != null)
                n_img++;
        }

        return n_img;
    }
}
