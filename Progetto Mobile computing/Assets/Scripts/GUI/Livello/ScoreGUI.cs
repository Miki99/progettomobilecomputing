﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreGUI : MonoBehaviour
{
    private Text scoreTxt;
    GameObject levelManager;

    // Start is called before the first frame update
    void Start()
    {
        scoreTxt = GetComponent<Text>();
        levelManager = GameObject.FindGameObjectWithTag("level_manager").gameObject;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        scoreTxt.text = "Punteggio\n" + levelManager.GetComponent<LevelManager>().GetPunteggioTotaleAttuale();
    }
}
