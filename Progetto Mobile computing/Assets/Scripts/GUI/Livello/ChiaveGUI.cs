﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChiaveGUI : MonoBehaviour
{
    private GameObject player;
    private Image imgChiave;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("player").gameObject;
        imgChiave = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.GetComponent<Player>().HaveKey())
            imgChiave.enabled = true;
    }
}
