﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TempoRimanenteGUI : MonoBehaviour
{
    Text txtTempoRimanente;
    LevelManager levelManager;                          //Level manager

    // Start is called before the first frame update
    void Start()
    {
        txtTempoRimanente = GetComponent<Text>();
        levelManager = GameObject.FindGameObjectWithTag("level_manager").gameObject.GetComponent<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {
        txtTempoRimanente.text = "Timer\n" + formatoTempo(levelManager.TempoRimasto());
    }

    string formatoTempo(float tempo)
    {
        string minuti = ((int)(tempo / 60)) + "'";
        float tmp = tempo % 60;

        string secondi =  tmp.ToString("00.0") + "\"";

        return minuti + " " + secondi;
    }
}
