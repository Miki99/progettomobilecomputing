﻿using Firebase;
using Firebase.Auth;
using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GestoreScenaClassifica : MonoBehaviour
{
    public Text txt_primoPostoUsername;
    public Text txt_primoPostoPunteggio;
    public Text txt_secondoPostoUsername;
    public Text txt_secondoPostoPunteggio;
    public Text txt_terzoPostoUsername;
    public Text txt_terzoPostoPunteggio;
    public Text txt_quartoPostoUsername;
    public Text txt_quartoPostoPunteggio;
    public Text txt_quintoPostoUsername;
    public Text txt_quintoPostoPunteggio;
    public Text txt_migliorPunteggioUtente;

    Task<DataSnapshot> dbReadTask;

    void Start()
    {
        Classifica();
    }

    void Classifica()
    {
        dbReadTask = GManager.gameManager.dbReadClassificaTask;
        if (dbReadTask.Exception != null)
        {
            Debug.LogError($"Errore nella lettura di tutti i punteggi degli utenti. Eccezione: {dbReadTask.Exception}");
        }
        else
        {
            DataSnapshot ds = dbReadTask.Result;
            FirebaseUser utente = GManager.gameManager.utente;
            long posizione = ds.ChildrenCount;

            foreach(DataSnapshot punteggioDiUtente in ds.Children)
            {
                if(utente.DisplayName.Equals(punteggioDiUtente.Child("username").Value.ToString()))
                {
                    txt_migliorPunteggioUtente.text = punteggioDiUtente.Child("punteggio").Value.ToString();
                }
                switch (posizione)
                {
                    case 1:
                        txt_primoPostoUsername.text = punteggioDiUtente.Child("username").Value.ToString();
                        txt_primoPostoPunteggio.text = punteggioDiUtente.Child("punteggio").Value.ToString();
                        break;
                    case 2:
                        txt_secondoPostoUsername.text = punteggioDiUtente.Child("username").Value.ToString();
                        txt_secondoPostoPunteggio.text = punteggioDiUtente.Child("punteggio").Value.ToString();
                        break;
                    case 3:
                        txt_terzoPostoUsername.text = punteggioDiUtente.Child("username").Value.ToString();
                        txt_terzoPostoPunteggio.text = punteggioDiUtente.Child("punteggio").Value.ToString();
                        break;
                    case 4:
                        txt_quartoPostoUsername.text = punteggioDiUtente.Child("username").Value.ToString();
                        txt_quartoPostoPunteggio.text = punteggioDiUtente.Child("punteggio").Value.ToString();
                        break;
                    case 5:
                        txt_quintoPostoUsername.text = punteggioDiUtente.Child("username").Value.ToString();
                        txt_quintoPostoPunteggio.text = punteggioDiUtente.Child("punteggio").Value.ToString();
                        break;
                }

                posizione--;
            }
        }
    }

    public void PulsanteIndietro()
    {
        SceneManager.LoadScene("MenuPrincipale");
    }
}
