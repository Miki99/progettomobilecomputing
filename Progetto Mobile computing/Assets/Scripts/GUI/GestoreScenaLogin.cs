﻿using Firebase;
using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GestoreScenaLogin : MonoBehaviour
{
    public InputField iptfld_email;
    public InputField iptfld_password;
    public Text txt_msgCredenzialiErrate;
    public GameObject pnl_msgCredenzialiErrate;
    
    FirebaseAuth auth;

    void Awake()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            DependencyStatus dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                print("- GestoreScenaLogin: Impostato Firebase Auth");
                auth = FirebaseAuth.DefaultInstance;
                print("auth != null " + (auth != null));
            }
            else
            {
                print("- GestoreScenaLogin: Non risolto tutte le dipendeze Firebase: " + dependencyStatus);
            }
        });
    }

    //Quando il pulsante Accedi viene premuto, viene effettuato l'autenticazione.
    public void PulsanteAccedi()
    {
        StartCoroutine(Accedi(iptfld_email.text, iptfld_password.text));
    }

    //Effettua l'autenticazione tramite email e password.
    private IEnumerator Accedi(string email, string password)
    {
        var loginTask = auth.SignInWithEmailAndPasswordAsync(email, password);

        yield return new WaitUntil(predicate: () => loginTask.IsCompleted);

        if (loginTask.Exception != null)
        {
            //Utente non autenticato.
            Debug.LogWarning(message: $"Failed to register task with {loginTask.Exception}");

            FirebaseException firebaseEx = (FirebaseException) loginTask.Exception.GetBaseException();

            //Converte l'intero ad un valore dell'enumerazione di AuthError corrispondente.
            AuthError codiceErrore = (AuthError)firebaseEx.ErrorCode;

            string msg = "Autenticazione fallita!";
            switch (codiceErrore)
            {
                case AuthError.MissingEmail:
                    msg = "E-mail non inserita.";
                    break;
                case AuthError.MissingPassword:
                    msg = "Password non inserita.";
                    break;
                case AuthError.WrongPassword:
                    msg = "Password errata.";
                    break;
                case AuthError.InvalidEmail:
                    msg = "E-mail non valida.";
                    break;
                case AuthError.UserNotFound:
                    msg = "L'account non esiste";
                    break;
            }

            pnl_msgCredenzialiErrate.SetActive(true);
            txt_msgCredenzialiErrate.text = msg;
        }
        else
        {
            //Utente autenticato.
            FirebaseUser utente = loginTask.Result;
            Debug.LogFormat("User signed in successfully: {0}, ({1})", utente.DisplayName, utente.Email);
            
            GManager.gameManager.utente = utente;
            SceneManager.LoadScene("MenuPrincipale");
        }
    }

    //Il pulsante Indietro permette di tornare alla scherma iniziale del gioco.
    public void PulsanteIndietro()
    {
        SceneManager.LoadScene("SchermataIniziale");
    }

    //Il pulsante Chiudi chiude il popup con il messaggio di errore di autenticazione.
    public void PulsanteChiudi()
    {
        pnl_msgCredenzialiErrate.SetActive(false);
        txt_msgCredenzialiErrate.text = "";
    }
}
