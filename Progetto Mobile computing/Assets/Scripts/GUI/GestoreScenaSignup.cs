﻿using Firebase;
using Firebase.Auth;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GestoreScenaSignup : MonoBehaviour
{
    public InputField iptfld_username;
    public InputField iptfld_email;
    public InputField iptfld_password;
    public InputField iptfld_confermaPassword;
    public GameObject pnl_msgErrore;
    public Text txt_msgErrore;

    FirebaseAuth auth;

    void Awake()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            DependencyStatus dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                print("Impostato Firebase Auth");
                auth = FirebaseAuth.DefaultInstance;
            }
            else
            {
                print("Non risolto tutte le dipendeze Firebase: " + dependencyStatus);
            }
        });
    }

    //Il pulsante Registrati permette ad un utente di creare un nuovo profilo con i suoi dati relativi.
    public void PulsanteRegistrati()
    {
        StartCoroutine(Registrazione(iptfld_email.text, iptfld_password.text, iptfld_username.text));
    }

    private void PopupDiErroreConMessaggio(string msg)
    {
        pnl_msgErrore.SetActive(true);
        txt_msgErrore.text = msg;
    }

    private IEnumerator Registrazione(string email, string password, string username)
    {
        print("asdasfsf");
        //Campo username vuoto?
        if (username.Equals(""))
        {
            PopupDiErroreConMessaggio("Username non inserita.");
        }
        //Passoword inserita giusta?
        else if (iptfld_password.text != iptfld_confermaPassword.text)
        {
            PopupDiErroreConMessaggio("Password inserita e' errata.");
        }
        else
        {
            var registerTask = auth.CreateUserWithEmailAndPasswordAsync(email, password);
            
            yield return new WaitUntil(predicate: () => registerTask.IsCompleted);

            if (registerTask.Exception != null)
            {
                //Registrazione fallita.
                Debug.LogWarning(message: $"Failed to register task with {registerTask.Exception}");

                FirebaseException firebaseEx = (FirebaseException) registerTask.Exception.GetBaseException();

                //Viene convertito l'intero nel valore dell'enumerazione AuthError corrispondente.
                AuthError errorCode = (AuthError) firebaseEx.ErrorCode;

                string msg = "Registrazione fallita!";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        msg = "E-mail non inserita.";
                        break;
                    case AuthError.MissingPassword:
                        msg = "Password non inserita.";
                        break;
                    case AuthError.InvalidEmail:
                        msg = "E-mail non valida.";
                        break;
                    case AuthError.WeakPassword:
                        msg = "Password debole.";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        msg = "E-mail gia' usata.";
                        break;
                }

                PopupDiErroreConMessaggio(msg);
            }
            else
            {
                //Registrazione effettuata.
                FirebaseUser utente = registerTask.Result;

                UserProfile profiloUtente = new UserProfile();
                profiloUtente.DisplayName = username;

                //Imposta l'username al profilo dell'utente.
                var profileTask = utente.UpdateUserProfileAsync(profiloUtente);

                yield return new WaitUntil(predicate: () => profileTask.IsCompleted);

                if (profileTask.Exception != null)
                {
                    //Username non impostato.
                    Debug.LogWarning(message: $"Failed to register task with {profileTask.Exception}");

                    PopupDiErroreConMessaggio("Non e' stato possibile impostare lo usernane.");
                }
                else
                {
                    //Username impostato.
                    Debug.Log("Profilo creato.");
                    SceneManager.LoadScene("SchermataIniziale");
                }
            }
        }
    }

    //Il pulsante Indietro permette di tornare nella schermata iniziale del gioco.
    public void PulsanteIndietro()
    {
        SceneManager.LoadScene("SchermataIniziale");
    }

    //Il pulsante Chiudi chiude il popup dei messaggi di errore della registrazione.
    public void PulsanteChiudi()
    {
        pnl_msgErrore.SetActive(false);
        txt_msgErrore.text = "";
    }
}
