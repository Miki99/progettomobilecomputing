﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GestoreScenaSchermataIniziale : MonoBehaviour
{
    public GameObject pnl_credits;                      //Pannelo di Credits.
    public GameObject pnl_impostazioni;                 //Pannello delle impostazioni.
    public Slider sliderbarMusica;                      //Sliderbar di musica.
    public Slider sliderbarSuoni;                       //Sliderbar di suoni.

    private void Start()
    {
        sliderbarMusica.value = GManager.gameManager.livelloMusica;
        sliderbarSuoni.value = GManager.gameManager.livelloSuoni;

        Debug.Log("- GestoreScenaSchermataIniziale: utente != null ? " + (GManager.gameManager.utente != null));

        if((GManager.gameManager.utente != null))
            Debug.LogFormat("- GestoreScenaSchermataIniziale: Utente: {0}, ({1})", GManager.gameManager.utente.DisplayName, GManager.gameManager.utente.Email);
    }

    private void Update()
    {
        GManager.gameManager.GetComponent<AudioSource>().volume = sliderbarMusica.value;
    }

    //Il pulsante Accedi permette di andare nella scela Login per effettuare l'autenticazione.
    public void PulsanteAccedi()
    {
        SceneManager.LoadScene("Login");
    }

    //Il pulsante Registrati permette di andare nella scena Signup per creare un nuovo profilo.
    public void PulsanteRegistrati()
    {
        SceneManager.LoadScene("Signup");
    }

    public void PulsanteCredits()
    {
        pnl_credits.SetActive(true);
    }

    public void PulsanteOpzioni()
    {
        pnl_impostazioni.SetActive(true);
    }

    public void PulsanteChiudi()
    {
        GManager.gameManager.livelloMusica = sliderbarMusica.value;
        GManager.gameManager.livelloSuoni = sliderbarSuoni.value;
        GManager.gameManager.SalvaPreferenze();

        pnl_impostazioni.SetActive(false);
    }

    public void PulsanteChiudiDiPnlCredits()
    {
        pnl_credits.SetActive(false);
    }
}
