﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public bool destroy = false;                        //Indica se deve essere distrutto o meno questo game obejct.

    private float durataAnimazione;                     //Tempo di durata dell'animazione di explosion.
    private float tempoDurataAnim;                      //Tempo che scorre dell'animazione di explosion.
    private AudioSource explosionSound;                 //Suono dell'esplosione.
    private bool suonoAttivato = false;                 //Indica se il suono e' attivato o meno.

    // Start is called before the first frame update
    void Start()
    {
        durataAnimazione = GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length;
        explosionSound = GetComponent<AudioSource>();
        explosionSound.volume = GManager.gameManager.livelloSuoni;
        tempoDurataAnim = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(destroy && !suonoAttivato)
        {
            suonoAttivato = true;
            explosionSound.Play();
        }

        tempoDurataAnim += Time.fixedDeltaTime;

        //Distrugge questo game object quando finisce l'animazione e gli e' stato indicato.
        if ((tempoDurataAnim >= durataAnimazione) && destroy) {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Restituisce il tempo dell'animazione dell'esplizione.
    /// </summary>
    /// <returns>Tempo dell'animazione dell'esplizione.</returns>
    public float DurataAnimazioneEsplosione()
    {
        return durataAnimazione;
    }
}
